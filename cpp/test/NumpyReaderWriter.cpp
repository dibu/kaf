#include <iostream>
#include <string>

#include <KAF/Containers.hpp>
#include <KAF/Auxiliary.hpp>


int main(int argc, const char** argv)
{
  const std::string io_prefix = "NumpyReaderWriter/";
  KAF::NumpyReaderWriter numpy(io_prefix, io_prefix);

  KAF::DenseMatrix A {
    {1, 2, 3},
    {4, 5, 6}
  };

  const std::string A_filename = "A.npy";
  numpy.write(A_filename, A);

  std::cout << "written: " << A_filename << std::endl;
  std::cout << A << std::endl;

  /****************************************************************************/
  KAF::DenseMatrix B(numpy.read_matrix(A_filename));

  std::cout << "read: " << A_filename << std::endl;
  std::cout << B << std::endl;

  return 0;
}


