#include <vector> // vector
#include <string> // string
#include <algorithm> // generate, transform
#include <utility> // move
#include <cstdlib> // atof
#include <cmath> // pow

#include <cnpy.h>
#include <libconfig.hh>

#include <KAF/Containers.hpp>
#include <KAF/Partitioners.hpp>
#include <KAF/PairDistUtils.hpp>
#include <KAF/ManifoldLearning.hpp>
#include <KAF/Auxiliary.hpp>


std::vector<double> make_logarithmic_grid(
    const double left, const double right, const unsigned n_grid)
{
  std::vector<double> grid(n_grid);
  const double fraction = right / left;

  auto log_grid_generator = [&left, &fraction, &n_grid, n = 0] () mutable
  {
    return left * std::pow(fraction, double(n++) / (n_grid-1));
  };

  std::generate(grid.begin(), grid.end(), log_grid_generator);
  return grid;
}

int main(int argc, const char** argv)
{
  /****************************************************************************/
  using KAF::info, KAF::warn, KAF::error;
  KAF::Timer global_timer;

  info.newline();
  info.raw() << "**************************************\n";
  info.raw() << "* Manifold landscape via brute-force *\n";
  info.raw() << "**************************************\n";
  info.newline();

  libconfig::Config config; // this class throws like crazy; probably needs wrappers
  config.readFile("the_kaf.cfg");

  const std::string INPUT_PREFIX  = config.lookup("input-output.INPUT_PREFIX");
  const std::string OUTPUT_PREFIX = config.lookup("input-output.OUTPUT_PREFIX");
  const bool READ_BUNCH       = config.lookup("input-output.READ_BUNCH");
  const bool READ_LEARN_MAIN_DISTANCES =
    config.lookup("input-output.READ_LEARN_MAIN_DISTANCES");

  const bool WRITE_LEARN_MAIN_DISTANCES =
    config.lookup("input-output.WRITE_LEARN_MAIN_DISTANCES");

  const bool OBSERVE_ALL = config.lookup("timeseries.OBSERVE_ALL");

  const unsigned DELAY = config.lookup("delay-embedding.DELAY");

  const std::string KERNEL_TYPE = config.lookup("kernel.KERNEL_TYPE");

  const unsigned N_TUNE_MAIN = config.lookup("tune.N_TUNE_MAIN");
  const unsigned N_FORECAST_STEPS = config.lookup("forecast.N_FORECAST_STEPS");

  /****************************************************************************/
  const KAF::NumpyReaderWriter numpy(INPUT_PREFIX, OUTPUT_PREFIX);

  KAF::TimeSeriesBunch data;

  info.set_header_width(30);

  if (READ_BUNCH) {
    const std::string BUNCH_NAME = config.lookup("input-output.BUNCH_NAME");
    data = numpy.read_ts_bunch(BUNCH_NAME);

    info("Bunch file read: ", BUNCH_NAME);

  } else {
    const std::string SERIES_NAME = config.lookup("input-output.SERIES_NAME");
    const std::string TIME_NAME   = config.lookup("input-output.TIME_NAME");
    data.push_back(numpy.read_timeseries(SERIES_NAME, TIME_NAME));

    info("Series file read: ", SERIES_NAME);
    info("Time file read: ", TIME_NAME);
  }

  const auto n_total = data.count_vectors();

  if (!OBSERVE_ALL) {
    const unsigned OBSERVE_FROM_COORDINATE =
      config.lookup("timeseries.OBSERVE_FROM_COORDINATE");
    const unsigned OBSERVE_TO_COORDINATE =
      config.lookup("timeseries.OBSERVE_TO_COORDINATE");

    data = data.coordinate_slice(
        OBSERVE_FROM_COORDINATE, OBSERVE_TO_COORDINATE);
  }

  /*
   * The whole timeseries is split into four parts:
   * |         learn          |      tune      |
   * |------------------|-----|----------|-----|
   * |       main       |extra|   main   |extra|
   *
   * Here, extra == N_FORECAST_STEPS for both 'learn' and 'tune', and n_tune is
   * computed as N_TUNE_MAIN + N_FORECAST_STEPS.
   */

  KAF::LearnTunePartition learn_tune_partition(
      N_TUNE_MAIN,
      N_FORECAST_STEPS,
      DELAY);

  auto [ b_learn_main, b_tune_main ] =
    learn_tune_partition.cut_data( std::move(data) );

  {
    /* delay-embedding ********************************************************/
    b_learn_main = b_learn_main.generate_delay_embedded_bunch(DELAY);
    b_tune_main  = b_tune_main. generate_delay_embedded_bunch(DELAY);
  }

  auto learn_main = b_learn_main.convert_to_series_and_empty();
  auto tune_main  = b_tune_main. convert_to_series_and_empty();

  /****************************************************************************/
  info.set_header_width(15);
  info("total     : ", n_total);
  info("learn_main: ", learn_main.size());
  info("tune_main : ",  tune_main.size());
  info.set_header_width(30);

  info.newline();

  /****************************************************************************/
  KAF::Timer timer;

  auto&& learn_main_distances = KAF::PairDistFetcher(
      numpy,
      {
        "learn_main_distances.npy",
        WRITE_LEARN_MAIN_DISTANCES,
        READ_LEARN_MAIN_DISTANCES,
        true
      },
      learn_main)
    .fetch();

  info("Pairwise distances: ", timer);

  /****************************************************************************/
  timer.restart();

  if (KERNEL_TYPE != "VB") {
    error("kernel type ", KERNEL_TYPE, " is not VB, no manifold estimation");
    return 1;
  }
  if (argc < 4) {
    error("Not enough arguments provided: ", argc);
    return 1;
  }

  const double epsilon_left  = std::atof(argv[1]);
  const double epsilon_right = std::atof(argv[2]);
  const int    n_steps       = std::atoi(argv[3]);
  if (epsilon_left <= 0.0 || epsilon_right <= 0.0) {
    error("Epsilon interval should lie in R+: ",
        epsilon_left, ", ", epsilon_right);
    return 1;
  }
  if (n_steps < 1 || n_steps > 1000) {
    error("Number of steps should lie 1..1000: ", n_steps);
    return 1;
  }

  KAF::KernelSumSlope slope_computer(learn_main_distances);
  std::vector<double> landscape(n_steps);
  auto grid = make_logarithmic_grid(epsilon_left, epsilon_right, n_steps);

  auto compute_dimension = [&slope_computer](const double epsilon)
  {
    return slope_computer.compute_dimension(epsilon);
  };

  std::transform(
      grid.begin(), grid.end(), landscape.begin(), compute_dimension);


  numpy.write("grid.npy", grid);
  numpy.write("landscape.npy", landscape);

  info("Total time: ", global_timer);

  return 0;
}


