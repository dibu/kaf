import numpy as np
from matplotlib import pyplot as plt

def cut_ic_trajectory(timeseries, delay, start=0, steps=1):
  """
  Cut IC trajectory from a timeseries with specified delay, start and steps

  In:
    timeseries (np.array): 1-d or 2-d timeseries array
    delay (int>=0): delay-embedding dimension, can be 0 or positive
    start (int>=0): index of the timestep from which prediction is performed
    steps (int>=1): how many timesteps to put into the IC trajectory
  Out:
    ic_trajectory (np.array): ((delay+1)*d_vector,) or ((delay+1)*d_vector,steps)
                              array, flattened from timeseries in Fortran order
  """
  if steps == 1:
    ic_trajectory = cut_ic(timeseries, delay, start)
  elif steps > 1:
    d_vector = timeseries.shape[0] if timeseries.ndim == 2 else 1
    ic_trajectory = np.empty( ((delay+1)*d_vector, steps) )
    for k in range(steps):
      ic_trajectory[:,k] = cut_ic(timeseries, delay, start+k)
  else:
    raise ValueError('steps parameter is <= 0')

  return ic_trajectory


def cut_ic(timeseries, delay, start=0):
  """
  Cut IC from a timeseries with specified delay and start

  In:
    timeseries (np.array): 1-d or 2-d timeseries array
    delay (int): delay-embedding dimension, can be 0 or positive
    start (int): index of the timestep from which prediction is performed
  Out:
    ic (np.array): 1-d, size (delay+1)*d_vector, flattened in Fortran order
  """
  if delay > start:
    raise IndexError('delay > start, cannot cut IC')

  if timeseries.ndim == 1:
    ic = timeseries[start - delay : start + 1]
  else:
    ic = timeseries[:, start - delay : start + 1].ravel(order='f')

  return ic


def cut_test(timeseries, length, start=0):
  """
  Cut test chunk from a timeseries with specified length and start

  In:
    timeseries (np.array): 1-d or 2-d timeseries array
    length (int): number of timesteps to cut
    start (int): index of the timestep from which prediction is performed
  Out:
    test (np.array): (length,) or (d_vector,length) array
  """
  if timeseries.ndim == 1:
    n_timesteps = timeseries.size
  else:
    n_timesteps = timeseries.shape[1]

  if start + length > n_timesteps:
    raise IndexError('start + length > n_timesteps, cannot cut test chunk')

  if timeseries.ndim == 1:
    test = timeseries[start : start + length]
  else:
    test = timeseries[:, start : start + length]

  return test


