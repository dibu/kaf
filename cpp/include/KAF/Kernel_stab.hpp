class AbstractMatrix {
protected:
  const size_t n_rows;
  const size_t n_cols;

public:
  AbstractMatrix() = delete;
  AbstractMatrix(size_t n) :
    n_rows(n), n_cols(n) {}
  AbstractMatrix(size_t n, size_t m) :
    n_rows(n), n_cols(m) {}

  virtual ~AbstractMatrix() = default;
  virtual double& operator[](const size_t k) = 0;
  virtual const double& operator[](const size_t k) const = 0;
}; // AbstractMatrix


class DenseMatrix_ : public AbstractMatrix {
private:
  std::vector<double> array;

public:
  DenseMatrix_(size_t n) :
    AbstractMatrix(n),
    array(n*n) {}

  DenseMatrix_(size_t n, size_t m) :
    AbstractMatrix(n, m),
    array(n*m) {}

  DenseMatrix_(DenseMatrix_&& src) :
    AbstractMatrix(src.n_rows, src.n_cols),
    array(std::move(src.array))
  {
    std::cout << "[DenseMatrix_] moving!" << std::endl;
  }

  DenseMatrix_(const DenseMatrix_& src) :
    AbstractMatrix(src.n_rows, src.n_cols),
    array(src.array.begin(), src.array.end())
  {
    std::cout << "[DenseMatrix_] copying!" << std::endl;
  }

  double& operator()(const size_t i, const size_t j)
  {
    return array[i + n_rows*j];
  }

  const double& operator()(const size_t i, const size_t j) const
  {
    return array[i + n_rows*j];
  }

  double& operator[](const size_t k) override
  {
    return array[k];
  }

  const double& operator[](const size_t k) const override
  {
    return array[k];
  }

  double* get_pointer()
  {
    return array.data();
  }

  const double* get_pointer() const
  {
    return array.data();
  }

  bool operator==(const DenseMatrix& matrix)
  {
    return std::equal(array.begin(), array.end(), matrix.data());
  }

}; // DenseMatrix_


class SymmetricDenseMatrix : public AbstractMatrix {
private:
  std::vector<double> array;

public:
  SymmetricDenseMatrix(size_t n) :
    AbstractMatrix(n),
    array( (n*n + n) / 2 ) {}

  SymmetricDenseMatrix(SymmetricDenseMatrix&& src) :
    AbstractMatrix(src.n_rows),
    array(std::move(src.array))
  {
    std::cout << "[SymmetricDenseMatrix] moving!" << std::endl;
  }

  SymmetricDenseMatrix(const SymmetricDenseMatrix& src) :
    AbstractMatrix(src.n_rows),
    array(src.array.begin(), src.array.end())
  {
    std::cout << "[SymmetricDenseMatrix] copying!" << std::endl;
  }

  double& operator[](const size_t k) override
  {
    return array[k];
  }

  const double& operator[](const size_t k) const override
  {
    return array[k];
  }

  double* get_pointer()
  {
    return array.data();
  }

  const double* get_pointer() const
  {
    return array.data();
  }

}; // SymmetricDenseMatrix


enum MatrixType {
  DENSE     = (1 << 0),
  SYMMETRIC = (1 << 1)
};

inline MatrixType operator|(MatrixType a, MatrixType b)
{
  return static_cast<MatrixType>(static_cast<int>(a) | static_cast<int>(b));
}

inline MatrixType operator^(MatrixType a, MatrixType b)
{
  return static_cast<MatrixType>(static_cast<int>(a) ^ static_cast<int>(b));
}

class LinearMap {
private:
  MatrixType flags;
  std::unique_ptr<AbstractMatrix> p_matrix;

public:
  LinearMap() = delete;

  LinearMap(size_t n, const MatrixType flags = DENSE) :
    flags(flags)
  {
    if (flags ^ DENSE) {
      if (flags ^ SYMMETRIC) {
        p_matrix = std::make_unique<SymmetricDenseMatrix>(n);
      } else {
        p_matrix = std::make_unique<DenseMatrix_>(n);
      }
    } else {
      throw std::runtime_error("Not implemented");
    }
  }

  LinearMap(size_t n, size_t m, const MatrixType flags = DENSE) :
    flags(flags)
  {
    if (flags ^ DENSE) {
      if (flags ^ SYMMETRIC) {
        throw std::logic_error(
            "[LinearMap::LinearMap] "
            "cannot be rectangular and symmetric");
      } else {
        p_matrix = std::make_unique<DenseMatrix_>(n, m);
      }
    } else {
      throw std::runtime_error("Not implemented");
    }
  }

  LinearMap(LinearMap&& src) :
    flags(src.flags),
    p_matrix(std::move(src.p_matrix))
  {
    std::cout << "[LinearMap] moving!" << std::endl;
  }

  LinearMap(const LinearMap& src) :
    flags(src.flags),
    p_matrix(src.p_matrix)
  {
    std::cout << "[LinearMap] copying!" << std::endl;
  }

  double& operator()(const size_t i, const size_t j)
  {
    return p_matrix->operator()(i,j);
  }

  const double& operator()(const size_t i, const size_t j) const
  {
    return p_matrix->operator()(i,j);
  }

  double& operator[](const size_t k)
  {
    return p_matrix->operator[](k);
  }

  const double& operator[](const size_t k) const
  {
    return p_matrix->operator[](k);
  }

  double* get_pointer()
  {
    return p_matrix->get_pointer();
  }

  const double* get_pointer() const
  {
    return p_matrix->get_pointer();
  }

  bool operator==(const DenseMatrix& matrix)
  {
    return p_matrix->operator==(matrix);
  }

}; // class LinearMap



// in KernelBrain:
  template <typename Data>
  LinearMap compute_dense_matrix(const Data& data) const
  {
    auto n_points = data.get_points_count();

    LinearMap kernel_matrix(n_points);

    #pragma omp parallel for
    for (size_t j = 0; j < n_points; ++j) {
      for (size_t i = j; i < n_points; ++i) {
        const auto value = p_kernel->evaluate(data, i, j);
        kernel_matrix(i,j) = value;
        kernel_matrix(j,i) = value; // XXX delete to save space
      }
    }

    return kernel_matrix;
  }

