import numpy as np

def atleast_2d_transposed(xx):
  '''
  Ensure that returned array is at least 2d, and if it was 1d, transpose it.
  '''
  if xx.ndim == 1:
    return np.atleast_2d(xx).T
  return xx


def gaussian_kernel(length, xx, zz=None, metric='sqeuclidean'):
  '''
  Compute exp( -||x - y||^2 / length^2 )

  The 'length' parameter should be a real number, and 'xx' is a numpy array.
  Returns a square kernel matrix applied to pairwise distances.
  '''
  from sklearn.metrics.pairwise import pairwise_distances

  if zz is None:
    W = pairwise_distances(xx[:,np.newaxis], metric=metric)
  else:
    W = pairwise_distances(xx[:,np.newaxis], zz[:,np.newaxis], metric=metric)

  W /= length*length

  return np.exp(-W)


def anisotropic_gaussian_kernel(
    length_x, xx,
    length_r, rr,
    zz_x=None,
    zz_r=None,
    metric_x='sqeuclidean',
    metric_r='sqeuclidean'):
  '''
  Compute exp( -||xx - zz_x||^2 / length_x^2  -||rr - zz_r||^2 / length_r^2 )

  The 'length_{x,r}' parameters should be real numbers, and 'xx', 'rr' 1d-numpy
  arrays of the same size.

  Returns a square kernel matrix applied to pairwise distances if 'zz_{x,r}' are
  not provided.
  Returns a rectangular matrix otherwise.
  '''
  from scipy.spatial.distance import pdist, cdist, squareform

  xx = atleast_2d_transposed(xx)
  rr = atleast_2d_transposed(rr)

  if zz_x is None or zz_r is None:
    Wx = pdist(xx, metric=metric_x)
    Wr = pdist(rr, metric=metric_r)

    Wx /= length_x * length_x
    Wr /= length_r * length_r

    W = np.exp(- (Wx + Wr))
    W = squareform(W)
    np.fill_diagonal(W, 1)
  else:
    zz_x = atleast_2d_transposed(zz_x)
    zz_r = atleast_2d_transposed(zz_r)

    Wx = cdist(xx, zz_x, metric=metric_x)
    Wr = cdist(rr, zz_r, metric=metric_r)

    Wx /= length_x * length_x
    Wr /= length_r * length_r

    W = np.exp(- (Wx + Wr))

  return W


def compute_G_normalization_from(K):
  '''
  Compute KAF (G) kernel matrix from the GP (K) one

  G = D^{-1} @ K @ Q^{-1} @ K @ D^{-1}
  '''
  n_points = K.shape[0]

  d = K @ np.ones(n_points)
  D_inv = np.diag(1/d)

  q = K @ D_inv @ np.ones(n_points)
  Q_inv = np.diag(1/q)

  G = D_inv @ K @ Q_inv @ K @ D_inv
  
  G = 0.5 * (G + G.T)
  return G


def compute_KG_matrices(xx, kernel_func):
  '''
  Compute GP (K) and KAF (G) kernel matrices

  (K_ij) = kernel_func(xi, xj)
  G = D^{-1} @ K @ Q^{-1} @ K @ D^{-1}
  '''
  K = kernel_func(xx)
  K = 0.5 * (K + K.T)

  G = compute_G_normalization_from(K)

  return K, G


def compute_anisotropic_KG_matrices(xx, rr, kernel_func):
  '''
  Compute anisotropic (two lengthscales) GP (K) and KAF (G) kernel matrices

  (K_ij) = kernel_func(xi, xj, ri, rj)
  G = D^{-1} @ K @ Q^{-1} @ K @ D^{-1}
  '''
  K = kernel_func(xx, rr)
  K = 0.5 * (K + K.T)

  G = compute_G_normalization_from(K)

  return K, G


def compute_pseudo_inverse(eigvec, lam, truncation=None):
  '''
  Compute spectral pseudo-inverse of a matrix

  For a symmetric matrix, A = U @ diag(lam) @ U^T, pseudo-inverse is computed as
    A^† = U @ diag(1/lam) @ U^T

  If truncation is a positive integer (0, N], where N is the dimension, then
  only that many eigen-pairs are used.

  Resulting matrix is also normalized with 1/N, consistent with 'L^2'
  normalization of the eigen-vectors.
  
  '''

  n_points = lam.size
  m = lam.size if truncation is None else truncation

  A_dagger = eigvec[:,:m] @ np.diag(1/lam[:m]) @ eigvec[:,:m].T / n_points

  return A_dagger


def normalize_vectors(vectors, norm_method):
  '''
  Normalize vectors: max and min values are in [-1,1]

  If offset is not 0, then take into account only [offset:-offset] interval
  '''
  #return vectors / np.max(np.abs(vectors[offset:-offset]), axis=0)
  if norm_method == 'L^infinity':
    return vectors / np.linalg.norm(vectors, axis=0, ord=np.inf)
  elif norm_method == 'L^2':
    return vectors / np.linalg.norm(vectors, axis=0) * np.sqrt(vectors.shape[1])

  print("normalize_vectors: norm_method ==", norm_method, "not supported")
  return vectors


def xi(k, xx, c=1.0):
  '''
  "Theory"-predicted eigenfuncs for the unnorm. Laplacian with uniform density
  '''
  if k % 2 == 0:
    return np.sin(0.5 * np.pi * k * c * xx)
  else:
    return np.cos(0.5 * np.pi * k * c * xx)


def phi(k, xx):
  '''
  Theory-predicted eigenfuncs for the normalized Laplacian w/ uniform density
  '''
  left_value  = 1
  right_value = 1 if (k % 2 == 0) else -1

  mask_left  = xx < -1
  mask_right = xx > 1

  yy = np.cos(np.pi * k * (0.5*xx + 0.5))
  yy[mask_left]  =  left_value
  yy[mask_right] = right_value

  return yy


def compute_and_normalize_eigenvecs(
    L, norm_method='L^infinity', with_eigvals=True):
  '''
  Computes eigenvectors (and eigenvalues), and normalizes them (L^inf/L^2)
  '''
  lam, eigenvecs = np.linalg.eig(L)
  lam       = np.real(lam)
  eigenvecs = np.real(eigenvecs)
  eigenvecs = normalize_vectors(eigenvecs, norm_method=norm_method)

  return eigenvecs, lam if with_eigvals else eigenvecs


def compute_residues(L, eigvec_L, lam_L, truncations):
  residues = np.zeros(truncations.size + 1)
  n = lam_L.size

  for i, M in enumerate(truncations):
    residues[i] = np.linalg.norm(
        L - (eigvec_L[:,:M] @ np.diag(lam_L[:M]) @ eigvec_L[:,:M].T) / n,
        ord='fro')

  residues[-1] = np.linalg.norm(
      L - (eigvec_L @ np.diag(lam_L) @ eigvec_L.T) / n,
      ord='fro')

  return residues


def phi_linear(k, A, xx):
  '''
  Theory-predicted eigenfuncs for the normalized Laplacian w/ PDF ρ(x) = A*x + 1/2
  '''
  left_value  = 1
  right_value = 1 if (k % 2 == 0) else -1

  mask_left  = xx < -1
  mask_right = xx > 1

  #yy = np.cos(0.125 * np.pi * k * (xx**2 + 4*xx + 3))
  yy = np.cos( 0.5 * np.pi * k * (xx + 1) * (A * (xx-1) + 1) )
  yy[mask_left]  =  left_value
  yy[mask_right] = right_value

  return yy


def inverse_cdf_linear(A, yy):
  '''
  Computes inverse CDF of a r.v. with PDF ρ(x) = A*x + 1/2
  '''
  if A == 0.0:
    return 2*yy - 1

  b = 1 / (2*A)
  D2 = b*b + 1 + (2*yy - 1) / A
  return -b + np.sign(A) * np.sqrt(D2)


def xi_quadratic(k, xx):
  '''
  "Theory"-predicted eigenfuncs for the unnorm. Laplacian with PDF ρ(x) = 3/8x^2 + 3/8
  '''
  if k % 2 == 0:
    return np.sin(0.0625 * np.pi * k * (xx**3 + 3*xx + 4))
  else:
    return np.cos(0.0625 * np.pi * k * (xx**3 + 3*xx + 4))


def phi_quadratic(k, xx):
  '''
  Theory-predicted eigenfuncs for the normalized Laplacian w/ PDF ρ(x) = 3/8x^2 + 3/8
  '''
  left_value  = 1
  right_value = 1 if (k % 2 == 0) else -1

  mask_left  = xx < -1
  mask_right = xx > 1

  yy = np.cos(0.125 * np.pi * k * (xx**3 + 3*xx + 4))
  yy[mask_left]  =  left_value
  yy[mask_right] = right_value

  return yy


def inverse_cdf_quadratic(yy):
  '''
  Computes inverse CDF of a r.v. with PDF ρ(x) = 3/8x^2 + 3/8
  '''
  rr = np.sqrt(16*(yy**2 - yy) + 5)
  zz = np.power(rr - 4*yy + 2, 1/3)
  return 1/zz - zz



