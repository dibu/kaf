#pragma once

namespace KAF {

class GaussianKernel {
  /*
   * The standard Gaussian kernel:
   *    exp( - |x - y|^2 / ε )
   */
private:
  const double epsilon;

public:
  GaussianKernel() = delete;

  GaussianKernel(const double epsilon) :
    epsilon(epsilon) {}

  double evaluate(const double x) const
  {
    return exp( -x / epsilon);
  }

}; // class GaussianKernel


class GaussianKernelWithoutScale {
  /*
   * Like the standard Gaussian kernel, but without scaling:
   *    exp( - |x - y|^2 )
   */
public:
  static double evaluate(const double x)
  {
    return exp( -x );
  }

}; // class GaussianKernelWithoutScale

} // namespace KAF


