#pragma once

#include <string> // string, to_string
#include <memory> // unique_ptr, make_unique
#include <utility> // move
#include <stdexcept> // logic_error

#include <KAF/Containers.hpp>
#include <KAF/Auxiliary.hpp>


namespace KAF {

struct PairDistFetcherConfig {
  std::string filename;
  bool wants_to_save         = false;
  bool wants_to_load         = false;
  bool wants_to_be_symmetric = false;
}; // struct PairDistFetcherConfig


class PairDistFetcher {
  /*
   * A Builder (GoF) class that loads or computes PairwiseDistances object from
   * `Series` or `(Series, Series)`, and then optionally saves it to disk.
   *
   * An object of this class can be used multiple times, but it is designed to
   * only fetch one particular pairwise-distance matrix, i.e. user should create
   * a new object for each matrix s/he wants to fetch.
   */
private:
  const NumpyReaderWriter numpy;
  const PairDistFetcherConfig cfg;

  std::unique_ptr<Series> p_argument_one;
  std::unique_ptr<Series> p_argument_two;

  /* private methods **********************************************************/
  PairwiseDistances load_or_compute_pair_distances() const
  {
    // symmetric case
    if (cfg.wants_to_be_symmetric) {
      if (p_argument_one && p_argument_two) {
        warn << "[PairDistFetcher::load_or_compute_pair_distances()]\n";
        warn << "both arguments are set, but config wants distances to be"
             << "symmetric; will use the first argument\n";
      }
      if (p_argument_one) {
        return load_or_compute_pair_distances(*p_argument_one);
      } else if (p_argument_two) {
        return load_or_compute_pair_distances(*p_argument_two);
      } else {
        throw std::logic_error(
            "[PairDistFetcher::load_or_compute_pair_distances()] "
            "symmetric case, none of the arguments are set");
      }
    }

    // non-symmetric case
    if (!p_argument_one || !p_argument_two) {
      throw std::logic_error(
          "[PairDistFetcher::load_or_compute_pair_distances()] "
          "non-symmetric, and at least one argument is not set: "
          + std::to_string(!!p_argument_one)
          + ", "
          + std::to_string(!!p_argument_two));
    }
    return load_or_compute_pair_distances(*p_argument_one, *p_argument_two);
  }

  PairwiseDistances load_or_compute_pair_distances(const Series& series) const
  {
    if (!cfg.wants_to_be_symmetric) {
      throw std::logic_error(
          "[PairDistFetcher::load_or_compute_pair_distances(const Series&)] "
          "trying to fetch distances with one argument, but config is set to "
          "symmetric");
    }

    if (cfg.wants_to_load) {
      info("Loading: ", cfg.filename);
      return { numpy.read_matrix(cfg.filename), true };
    } else {
      info("Computing pairwise distances...");
      return { series };
    }
  }

  PairwiseDistances load_or_compute_pair_distances(
      const Series& series1,
      const Series& series2) const
  {
    if (cfg.wants_to_be_symmetric) {
      throw std::logic_error(
          "[PairDistFetcher::load_or_compute_pair_distances(const Series&, const Series&)] "
          "trying to fetch distances with two arguments, but config is set to "
          "non-symmetric");
    }

    if (cfg.wants_to_load) {
      info("Loading: ", cfg.filename);
      return { numpy.read_matrix(cfg.filename), false };
    } else {
      info("Computing pairwise distances...");
      return { series1, series2 };
    }
  }

  void save_pair_distances_if_needed(const PairwiseDistances& distances) const
  {
    if (cfg.wants_to_save) {
      if (cfg.wants_to_load) {
        warn("Will not save " + cfg.filename + " because it was loaded");
      } else {
        info("Saving: ", cfg.filename);
        numpy.write(cfg.filename, distances);
      }
    }
  }

public:
  PairDistFetcher() {}

  PairDistFetcher(NumpyReaderWriter numpy) :
    numpy(std::move(numpy)) {}

  PairDistFetcher(NumpyReaderWriter numpy, PairDistFetcherConfig cfg) :
    numpy(std::move(numpy)),
    cfg(std::move(cfg)) {}

  PairDistFetcher(
      NumpyReaderWriter numpy,
      PairDistFetcherConfig cfg,
      Series series) :
    numpy(std::move(numpy)),
    cfg(std::move(cfg))
  {
    set_argument1(std::move(series));
  }

  void set_argument1(Series series)
  {
    if (p_argument_one) {
      throw std::logic_error(
          "[PairDistFetcher::set_argument1] argument_one was already set");
    }
    p_argument_one = std::make_unique<Series>(std::move(series));
  }

  void set_argument2(Series series)
  {
    if (p_argument_two) {
      throw std::logic_error(
          "[PairDistFetcher::set_argument2] argument_two was already set");
    }
    p_argument_two = std::make_unique<Series>(std::move(series));
  }

  PairwiseDistances fetch() const
  {
    auto&& pair_distances = load_or_compute_pair_distances();
    save_pair_distances_if_needed(pair_distances);

    return pair_distances;
  }

}; // struct PairDistFetcher


class PairDistAssembler {
  /*
   * A Factory (GoF) class that prepares a PairwiseDistances object for
   * KernelEvaluator, with fetching options for raw pairwise distances.
   */
private:
  class PairDistRescalerConcept;
  template <typename Rescaler> class PairDistRescalerModel;

  Series learn_series;
  std::unique_ptr<PairDistRescalerConcept> p_pairdist_rescaler;

public:
  template <typename Rescaler>
  PairDistAssembler(Series learn_series, Rescaler pairdist_rescaler) :
    learn_series(std::move(learn_series)),
    p_pairdist_rescaler
    (
     std::make_unique<PairDistRescalerModel<Rescaler> >
     (
      std::move(pairdist_rescaler)
     )
    ) {}

  PairwiseDistances operator()(PairDistFetcher&& fetcher) const
  {
    fetcher.set_argument1(learn_series);
    auto&& pair_distances = fetcher.fetch();

    p_pairdist_rescaler->apply(pair_distances);

    return pair_distances;
  }

private:
  class PairDistRescalerConcept {
    public:
      virtual ~PairDistRescalerConcept() = default;
      virtual void apply(PairwiseDistances&) const = 0;
  }; // class PairDistRescalerConcept

  template <typename Rescaler>
  class PairDistRescalerModel : public PairDistRescalerConcept {
    private:
      Rescaler rescaler;

    public:
      PairDistRescalerModel(Rescaler rescaler) :
        rescaler(std::move(rescaler)) {}

      void apply(PairwiseDistances& pair_distances) const final override
      {
        return rescaler.apply(pair_distances);
      }
  }; // class PairDistRescalerModel

}; // class PairDistAssembler

} // namespace KAF


