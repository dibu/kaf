#include <string> // string
#include <utility> // move

#include <Eigen/Core>

#include <cnpy.h>
#include <libconfig.hh>

#include <KAF/Containers.hpp>
#include <KAF/Partitioners.hpp>
#include <KAF/LinearAlgebra.hpp>
#include <KAF/Kernels.hpp>
#include <KAF/PairDistUtils.hpp>
#include <KAF/PairDistRescalers.hpp>
#include <KAF/ManifoldLearning.hpp>
#include <KAF/DiffusionOperator.hpp>
#include <KAF/Forecaster.hpp>
#include <KAF/Auxiliary.hpp>


KAF::Series load_ic(
    const KAF::NumpyReaderWriter& numpy,
    const libconfig::Config& cfg)
{
    const std::string IC_FILE = cfg.lookup("input-output.IC_FILE");
    KAF::Series ic = numpy.read_series(IC_FILE);
    KAF::info("IC file read: ", IC_FILE);

    return ic;
}

bool check_ic_is_ok(const KAF::Series& ic, const size_t desired_dimension)
{
  const auto d_ic = ic.get_vector_dimension();

  if (d_ic != desired_dimension) {
    KAF::error("Vector dimension of ic is not equal to the desired dimension: ",
        d_ic, ", ", desired_dimension);
    return false;
  }

  return true;
}


int main(int argc, const char** argv)
{
  /****************************************************************************/
  using KAF::info, KAF::warn, KAF::error;
  KAF::Timer global_timer;

  info.newline();
  info.raw() << "***********************************\n";
  info.raw() << "* Kernel analog forecasting v0.01 *\n";
  info.raw() << "***********************************\n";
  info.newline();

  libconfig::Config config; // this class throws like crazy; probably needs wrappers
  config.readFile("the_kaf.cfg");

  const std::string INPUT_PREFIX  = config.lookup("input-output.INPUT_PREFIX");
  const std::string OUTPUT_PREFIX = config.lookup("input-output.OUTPUT_PREFIX");
  const bool READ_BUNCH       = config.lookup("input-output.READ_BUNCH");
  const bool READ_MEASUREMENT = config.lookup("input-output.READ_MEASUREMENT");
  const bool READ_LEARN_MAIN_DISTANCES =
    config.lookup("input-output.READ_LEARN_MAIN_DISTANCES");
  const bool READ_IC_DISTANCES =
    config.lookup("input-output.READ_IC_DISTANCES");

  const bool WRITE_LEARN_MAIN_DISTANCES =
    config.lookup("input-output.WRITE_LEARN_MAIN_DISTANCES");
  const bool WRITE_IC_DISTANCES =
    config.lookup("input-output.WRITE_IC_DISTANCES");
  const bool WRITE_KERNEL_FIELD =
    config.lookup("input-output.WRITE_KERNEL_FIELD");

  const bool OBSERVE_ALL = config.lookup("timeseries.OBSERVE_ALL");

  const unsigned DELAY = config.lookup("delay-embedding.DELAY");

  const std::string KERNEL_TYPE = config.lookup("kernel.KERNEL_TYPE");
  const double SPARSITY_THRESHOLD_MAIN =
    config.lookup("kernel.SPARSITY_THRESHOLD_MAIN");
  const double SPARSITY_THRESHOLD_IC =
    config.lookup("kernel.SPARSITY_THRESHOLD_IC");
  const double SPARSE_DENSE_DECIDER =
    config.lookup("kernel.SPARSE_DENSE_DECIDER");
  const bool RUN_BANDWIDTH_DIMENSION_ESTIMATION =
    config.lookup("kernel.RUN_BANDWIDTH_DIMENSION_ESTIMATION");

  const unsigned N_EIG = config.lookup("eigen-basis.N_EIG");

  const bool DO_TUNING_1ST_MOMENT = config.lookup("tune.DO_TUNING_1ST_MOMENT");
  const bool DO_TUNING_2ND_MOMENT = config.lookup("tune.DO_TUNING_2ND_MOMENT");
  const unsigned N_TUNE_MAIN = config.lookup("tune.N_TUNE_MAIN");

  const unsigned N_FORECAST_STEPS = config.lookup("forecast.N_FORECAST_STEPS");
  const bool FORECAST_VARIANCE = config.lookup("forecast.FORECAST_VARIANCE");
  const bool FORECAST_2ND_MOMENT = config.lookup("forecast.FORECAST_2ND_MOMENT");

  /****************************************************************************/
  const KAF::NumpyReaderWriter numpy(INPUT_PREFIX, OUTPUT_PREFIX);

  KAF::TimeSeriesBunch data;
  KAF::TimeSeriesBunch measurement;

  info.set_header_width(30);

  if (READ_BUNCH) {
    const std::string BUNCH_NAME = config.lookup("input-output.BUNCH_NAME");
    data = numpy.read_ts_bunch(BUNCH_NAME);

    info("Bunch file read: ", BUNCH_NAME);

    if (READ_MEASUREMENT) {
      error("Cannot read measurement when reading a bunch file");
      return 1;
    }
  } else {
    const std::string SERIES_NAME = config.lookup("input-output.SERIES_NAME");
    const std::string TIME_NAME   = config.lookup("input-output.TIME_NAME");
    data.push_back(numpy.read_timeseries(SERIES_NAME, TIME_NAME));

    info("Series file read: ", SERIES_NAME);
    info("Time file read: ", TIME_NAME);

    if (READ_MEASUREMENT) {
      const std::string MEASUREMENT_NAME =
        config.lookup("input-output.MEASUREMENT_NAME");
      measurement.push_back(
          numpy.read_timeseries(MEASUREMENT_NAME, TIME_NAME));

      info("Measurement file read: ", MEASUREMENT_NAME);
    }
  }

  if (!READ_MEASUREMENT) {
    const unsigned MEASUREMENT_COORDINATE =
      config.lookup("timeseries.MEASUREMENT_COORDINATE");

    measurement = data.coordinate_slice(MEASUREMENT_COORDINATE);
  }

  const auto n_total = data.count_vectors();
  if (n_total != measurement.count_vectors()) {
    error("Number of vectors in data and measurement is not equal: ",
        n_total, ", ", measurement.count_vectors());
    return 1;
  }

  if (!OBSERVE_ALL) {
    const unsigned OBSERVE_FROM_COORDINATE =
      config.lookup("timeseries.OBSERVE_FROM_COORDINATE");
    const unsigned OBSERVE_TO_COORDINATE =
      config.lookup("timeseries.OBSERVE_TO_COORDINATE");

    data = data.coordinate_slice(
        OBSERVE_FROM_COORDINATE, OBSERVE_TO_COORDINATE);
  }

  /*
   * The whole timeseries is split into four parts:
   * |         learn          |      tune      |
   * |------------------|-----|----------|-----|
   * |       main       |extra|   main   |extra|
   *
   * Here, extra == N_FORECAST_STEPS for both 'learn' and 'tune', and n_tune is
   * computed as N_TUNE_MAIN + N_FORECAST_STEPS.
   */

  KAF::LearnTunePartition learn_tune_partition(
      N_TUNE_MAIN,
      N_FORECAST_STEPS,
      DELAY);

  auto [ b_learn_main, b_tune_main ] =
    learn_tune_partition.cut_data( std::move(data) );

  auto [ measurement_learn, measurement_tune ] =
    learn_tune_partition.cut_measurement( std::move(measurement) );

  {
    /* delay-embedding ********************************************************/
    b_learn_main = b_learn_main.generate_delay_embedded_bunch(DELAY);
    b_tune_main  = b_tune_main. generate_delay_embedded_bunch(DELAY);

    measurement_learn = measurement_learn.time_slice_from(DELAY);
    measurement_tune  = measurement_tune. time_slice_from(DELAY);
  }

  auto learn_main = b_learn_main.convert_to_series_and_empty();
  auto tune_main  = b_tune_main. convert_to_series_and_empty();

  measurement_learn.set_partition({ N_FORECAST_STEPS });
  measurement_tune. set_partition({ N_FORECAST_STEPS });

  /****************************************************************************/
  KAF::Series ic = load_ic(numpy, config);

  if (!check_ic_is_ok(ic, learn_main.get_vector_dimension())) {
    return 1;
  }

  /****************************************************************************/
  info.set_header_width(15);
  info("total     : ", n_total);
  info("learn     : ", measurement_learn.count_vectors());
  info("tune      : ", measurement_tune. count_vectors());
  info("learn_main: ", learn_main.size());
  info("tune_main : ",  tune_main.size());
  info("ic steps  : ", ic.size());
  info.set_header_width(30);

  info.newline();

  /****************************************************************************/
  KAF::Timer timer;

  auto&& learn_main_distances = KAF::PairDistFetcher(
      numpy,
      {
        "learn_main_distances.npy",
        WRITE_LEARN_MAIN_DISTANCES,
        READ_LEARN_MAIN_DISTANCES,
        true
      },
      learn_main)
    .fetch();

  info("Pairwise distances: ", timer);

  /****************************************************************************/
  timer.restart();

  auto&& pairdist_assembler = [&] () -> KAF::PairDistAssembler
  {
    if (KERNEL_TYPE == "VB") {
      const double KNN_RATIO     = config.lookup("kernel.vb.KNN_RATIO");
      const double EPSILON_GUESS = config.lookup("kernel.vb.EPSILON_GUESS");
      const double DELTA_GUESS = config.lookup("kernel.vb.DELTA_GUESS");

      KAF::KernelDensityEstimator kde(
          learn_main_distances,
          KNN_RATIO,
          RUN_BANDWIDTH_DIMENSION_ESTIMATION,
          EPSILON_GUESS);
      info("KDE parameters:", kde);
      numpy.write("rho.npy", Eigen::VectorXd(kde.get_rho()));

      learn_main_distances.rescale_by(kde.get_rho());

      double delta = 0.0;
      if (RUN_BANDWIDTH_DIMENSION_ESTIMATION) {
        delta = kde.estimate_bandwidth(learn_main_distances, DELTA_GUESS);
      } else {
        delta = DELTA_GUESS;
      }

      learn_main_distances.rescale_by(delta);

      info("Kernel density estimation: ", timer);

      return {
        std::move(learn_main),
        KAF::PairDistRescalerForVB(std::move(kde), delta)
      };
    } else if (KERNEL_TYPE == "RBF") {
      const double DELTA_GUESS = config.lookup("kernel.rbf.DELTA_GUESS");

      const double delta = [&] ()
      {
        if (RUN_BANDWIDTH_DIMENSION_ESTIMATION) {
          KAF::BandwidthDimensionOptimizer bandwidth_dimension;
          auto delta_and_dim = bandwidth_dimension(
              learn_main_distances, DELTA_GUESS);
          return delta_and_dim.first;
        } else {
          return DELTA_GUESS;
        }
      } ();

      learn_main_distances.rescale_by(delta);

      return { std::move(learn_main), KAF::PairDistRescalerForGaussian(delta) };
    } else {
      error("kernel type ", KERNEL_TYPE, " is not implemented");
      exit(1);
    }
  } ();

  KAF::KernelEvaluator kernel_evaluator(
      KAF::GaussianKernelWithoutScale(),
      std::move(pairdist_assembler));

  /****************************************************************************/
  timer.restart();

  const auto kernel_matrix = kernel_evaluator.compute_sparse_matrix(
      learn_main_distances, SPARSITY_THRESHOLD_MAIN);
  info("Kernel matrix: ", timer);

  /****************************************************************************/
  timer.restart();

  KAF::DiffusionOperator diffusion_operator(kernel_matrix, SPARSE_DENSE_DECIDER);
  info("Diffusion operator: ", timer);

  /****************************************************************************/
  timer.restart();

  diffusion_operator.compute_and_store_eigenbasis(N_EIG);
  info("Diffusion eigenbasis: ", timer);

  /****************************************************************************/
  timer.restart();

  KAF::PairDistFetcher ic_distances_fetcher(
      numpy,
      {"ic_distances.npy", WRITE_IC_DISTANCES, READ_IC_DISTANCES, false});
  ic_distances_fetcher.set_argument2(std::move(ic));

  const auto ic_kernel_matrix = kernel_evaluator.compute_sparse_matrix(
      std::move(ic_distances_fetcher), SPARSITY_THRESHOLD_IC);

  info("IC kernel matrix: ", timer);

  /****************************************************************************/
  timer.restart();

  const auto eigenfuncs_evaluated_on_ic =
    diffusion_operator.compute_nystrom_extension(
        ic_kernel_matrix, SPARSE_DENSE_DECIDER);
  info("Nystrom extension on IC: ", timer);

  /****************************************************************************/
  timer.restart();

  const auto eigenfuncs_evaluated_on_tune = [&] ()
  {
    if (DO_TUNING_1ST_MOMENT || DO_TUNING_2ND_MOMENT) {
      KAF::PairDistFetcher tune_main_distances_fetcher(numpy);
      tune_main_distances_fetcher.set_argument2(std::move(tune_main));

      const auto tune_kernel_matrix = kernel_evaluator.compute_sparse_matrix(
          std::move(tune_main_distances_fetcher), SPARSITY_THRESHOLD_MAIN);

      auto&& nystrom_extension = diffusion_operator.compute_nystrom_extension(
          tune_kernel_matrix, SPARSE_DENSE_DECIDER);

      info("Nystrom extension for tune timeseries: ", timer);
      return nystrom_extension;
    } else {
      return KAF::DenseMatrix();
    }
  } ();

  /****************************************************************************/
  timer.restart();

  KAF::Forecaster forecaster(diffusion_operator);

  if (DO_TUNING_1ST_MOMENT) {
    forecaster.compute_and_store_truncations(
        N_FORECAST_STEPS,
        eigenfuncs_evaluated_on_tune,
        measurement_tune,
        measurement_learn);
  } else {
    const unsigned TRUNCATION = config.lookup("tune.TRUNCATION");
    forecaster.set_truncations(N_FORECAST_STEPS, TRUNCATION);
  }

  const auto moment_1 = forecaster.forecast(
      N_FORECAST_STEPS,
      eigenfuncs_evaluated_on_ic,
      measurement_learn);

  info("Forecasted 1st moment: ", timer);

  /****************************************************************************/
  timer.restart();

  const auto moment_2 = [&] ()
  {
    if (FORECAST_2ND_MOMENT) {
      auto square = [] (const double* vector) -> double
      {
        return vector[0] * vector[0];
      };
      const auto measurement_squared_learn =
        measurement_learn.generate_bunch_by_applying(square);
      const auto measurement_squared_tune =
        measurement_tune.generate_bunch_by_applying(square);

      if (DO_TUNING_2ND_MOMENT) {
        forecaster.compute_and_store_truncations(
            N_FORECAST_STEPS,
            eigenfuncs_evaluated_on_tune,
            measurement_squared_tune,
            measurement_squared_learn);
      } else {
        const unsigned TRUNCATION = config.lookup("tune.TRUNCATION");
        forecaster.set_truncations(N_FORECAST_STEPS, TRUNCATION);
      }

      auto&& moment_2 = forecaster.forecast(
          N_FORECAST_STEPS,
          eigenfuncs_evaluated_on_ic,
          measurement_squared_learn);

      info("Forecasted 2nd moment: ", timer);
      return moment_2;
    } else {
      return KAF::DenseMatrix();
    }
  } ();

  /****************************************************************************/
  timer.restart();

  const auto variance = [&] ()
  {
    if (FORECAST_VARIANCE) {
      auto&& variance = forecaster.forecast_variance(
          N_FORECAST_STEPS,
          eigenfuncs_evaluated_on_ic,
          measurement_learn);

      info("Forecasted variance: ", timer);
      return variance;
    } else {
      return KAF::DenseMatrix();
    }
  } ();


  /****************************************************************************/
  // NOTE: all matrices below are written transposed

  numpy.write("moment_1.npy", moment_1);
  if (FORECAST_2ND_MOMENT) {
    numpy.write("moment_2.npy", moment_2);
  }
  if (FORECAST_VARIANCE) {
    numpy.write("variance.npy", variance);
  }

  numpy.write("ic_kernel_matrix.npy", ic_kernel_matrix);

  numpy.write("singvals.npy",     diffusion_operator.get_eigenvalues_sqrt());
  numpy.write("eigenvectors.npy", diffusion_operator.get_eigenvectors());

  if (WRITE_KERNEL_FIELD) {
    auto&& kernel_field = diffusion_operator.compute_kernel_field(
        ic_kernel_matrix, SPARSE_DENSE_DECIDER);

    numpy.write("kernel_field.npy", kernel_field);
  }

  /*
  numpy.write("kernel_matrix.npy", kernel_matrix);

  if (std::holds_alternative<KAF::DenseMatrix>(normalized_kernel_matrix)) {
    numpy.write("normalized_kernel.npy", std::get<KAF::DenseMatrix>(normalized_kernel_matrix));
  } else {
    numpy.write("normalized_kernel.npy", std::get<KAF::SparseMatrix>(normalized_kernel_matrix));
  }

  numpy.write("adhoc_density.npy", Eigen::VectorXd(kde.get_adhoc_density()));

  numpy.write("oos_kernel_matrix.npy", oos_kernel_matrix);
  */

  /****************************************************************************/
  info("Total time: ", global_timer);

  return 0;
}


