#pragma once

#include <string>
#include <unordered_map>
#include <ostream>
#include <iomanip>
#include <iterator> // size, begin
#include <numeric> // accumulate

#include <boost/numeric/odeint.hpp>

#include <KAF/Containers.hpp>


namespace KAF {

class ODEParameters {
private:
  std::unordered_map<std::string, double> parameter_dictionary;

public:
  template <typename Iterator1, typename Iterator2>
    ODEParameters(Iterator1 it1, Iterator2 it2, const size_t n_parameters)
  {
    for (size_t i = 0; i < n_parameters; ++i) {
      parameter_dictionary.emplace(*(it1++), *(it2++));
    }
  }

  double operator[](const std::string& key) const
  {
    return parameter_dictionary.at(key);
  }

  friend std::ostream&
    operator<<(std::ostream& out, const ODEParameters& parameters)
  {
    for (const auto& key_value_pair : parameters.parameter_dictionary) {
      out << key_value_pair.first << ": " << key_value_pair.second << std::endl;
    }

    return out;
  }

}; // class ODEParameters


/*
 * Tvec = { std::array | boost::array | Eigen::Matrix }
 */

template <typename Tvec>
class GenericODE {
  /*
   * An abstract class whose child-classes are supposed to hold RHS's of ODEs
   */
public:
  virtual Tvec compute_rhs(const double t, const Tvec& state,
                           const ODEParameters& parameters) const = 0;

}; // class GenericODE


template <typename Tvec>
class Lorenz63 : public GenericODE<Tvec> {
  /*
   * Lorenz '63 model implementation
   */
public:
  Tvec compute_rhs(
      const double t,
      const Tvec& state,
      const ODEParameters& l63_parameters) const final override
  {
    Tvec rhs;

    auto [ x, y, z ] = state;

    auto sigma = l63_parameters["sigma"];
    auto rho   = l63_parameters["rho"];
    auto beta  = l63_parameters["beta"];

    rhs[0] = sigma * (y - x);
    rhs[1] = x * (rho - z) - y;
    rhs[2] = x * y - beta * z;

    return rhs;
  }
}; // class Lorenz63


template <typename Tvec>
class Lorenz96 : public GenericODE<Tvec> {
  /*
   * Lorenz '96 model implementation
   */
private:
  const unsigned K;
  const unsigned J;

public:
  Lorenz96() = delete;
  Lorenz96(const unsigned K, const unsigned J) : K(K), J(J) {}

  Tvec compute_rhs(
      const double t,
      const Tvec& state,
      const ODEParameters& l96_parameters) const final override
  {
    Tvec rhs {state};
    auto& z = state; // for shorter notation in the formulas
    auto F   = l96_parameters["F"];
    auto hx  = l96_parameters["hx"];
    auto hy  = l96_parameters["hy"];
    auto eps = l96_parameters["eps"];

    /* SLOW x_k subsystem ***********************/
    // boundary cases
    rhs[0]   = -z[K-1] * (z[K-2] - z[1]) - z[0];
    rhs[1]   = -z[0]   * (z[K-1] - z[2]) - z[1];
    rhs[K-1] = -z[K-2] * (z[K-3] - z[0]) - z[K-1];
    // general case
    for (unsigned k = 2; k < K-1; ++k) {
      rhs[k] = -z[k-1] * (z[k-2] - z[k+1]) - z[k];
    }

    // add forcing and coupling
    Tvec Y = compute_fast_averages(z);
    for (unsigned k = 0; k < K; ++k) {
      rhs[k] += F + hx * Y[k];
    }

    /* FAST y_{j,k} subsystem *******************/
    // lambda computes index of the (j,k)-th fast variable in a flattened array
    auto _ = [&J = J, &K = K] (int j, int k) {
      auto JK = J*K;
      int idx = ( (k * J + j) + JK) % JK;
      return idx + K;
    };

    for (int k = 0; k < K; ++k) {
      auto slow_coupling = hy * z[k];
      for (int j = 0; j < J; ++j) {
        rhs[ _(j,k) ] =
          -z[ _(j+1,k) ] *
          (z[ _(j+2,k) ] - z[ _(j-1,k) ])
          -z[ _(j,k) ]
          + slow_coupling;
        rhs[ _(j,k) ] /= eps;
      }
    }

    return rhs;
  }

  Tvec compute_fast_averages(const Tvec& state) const
  {
    Tvec Y {state};
    auto slow_begin_iterator = std::begin(state) + K;

    for (unsigned k = 0; k < K; ++k) {
      Y[k] = std::accumulate(
          slow_begin_iterator + k*J,
          slow_begin_iterator + (k+1)*J,
          0.0);
      Y[k] /= J;
    }

    return Y;
  }
}; // class Lorenz96


template <typename Tvec>
class DoubleWell : public GenericODE<Tvec> {
  /*
   * Lorenz '63 with double-well gradient flow implementation
   */
public:
  Tvec compute_rhs(
      const double t,
      const Tvec& state,
      const ODEParameters& doublewell_parameters) const final override
  {
    Tvec rhs;

    auto [ x, y1, y2, y3 ] = state;

    auto sigma    = doublewell_parameters["sigma"];
    auto rho      = doublewell_parameters["rho"];
    auto beta     = doublewell_parameters["beta"];
    auto epsilon  = doublewell_parameters["epsilon"];
    auto forcing  = doublewell_parameters["forcing"];

    rhs[0] = x - x*x*x + forcing * y2 / epsilon;
    rhs[1] = sigma * (y2 - y1);
    rhs[2] = y1 * (rho - y3) - y2;
    rhs[3] = y1 * y2 - beta * y3;

    for (size_t i = 1; i < 4; ++i) {
      rhs[i] /= epsilon*epsilon;
    }

    return rhs;
  }
}; // class DoubleWell


class ODEIntegratorParameters {
  /*
   * A class that stores parameters for the ODEIntegrator class
   */
public:
  const double t0; // start time
  const double t1; // end time
  const double dt; // time step; depending on the context, either maximum,
                   // first, or constant time step
  const double abstol; // absolute tolerance
  const double reltol; // relative tolerance

  ODEIntegratorParameters(
      const double t0, const double t1, const double dt,
      const double abstol = 1e-7, const double reltol = 1e-4) :
    t0(t0), t1(t1), dt(dt), abstol(abstol), reltol(reltol) {}

  unsigned get_approximately_steps_count() const
  {
    return unsigned( (t1 - t0) / dt );
  }

  friend std::ostream& operator<<(
      std::ostream& out, const ODEIntegratorParameters& integrator_parameters)
  {
    out << "t0:     " << integrator_parameters.t0     << std::endl;
    out << "t1:     " << integrator_parameters.t1     << std::endl;
    out << "dt:     " << integrator_parameters.dt     << std::endl;
    out << "abstol: " << integrator_parameters.abstol << std::endl;
    out << "reltol: " << integrator_parameters.reltol << std::endl;

    return out;
  }

}; // class ODEIntegratorParameters


class ODEIntegrator {
  /*
   * A class that handles integration of ODEs
   */
private:
  ODEIntegratorParameters integrator_parameters;

public:
  ODEIntegrator(const ODEIntegratorParameters& integrator_parameters) :
    integrator_parameters(integrator_parameters) {}

  template <typename Tvec> TimeSeries run(
      Tvec initial_state,  // copy because odeint mutates this
      const GenericODE<Tvec>& ode,
      const ODEParameters& parameters)
  {
    using namespace boost::numeric::odeint;

    typedef runge_kutta_dopri5<Tvec> error_stepper_type;
    auto stepper = make_controlled(
        this->integrator_parameters.abstol,
        this->integrator_parameters.reltol,
        this->integrator_parameters.dt,
        error_stepper_type());

    const auto n_approximately_steps =
      integrator_parameters.get_approximately_steps_count();
    TimeSeries ode_result(std::size(initial_state), n_approximately_steps);

    const auto saved_precision = std::cout.precision();
    std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(2);

    integrate_const(
        stepper,
        [&ode, &parameters] (const Tvec& state, Tvec& rhs, const double t) {
          rhs = ode.compute_rhs(t, state, parameters);
        },
        initial_state,
        this->integrator_parameters.t0,
        this->integrator_parameters.t1,
        this->integrator_parameters.dt,
        [&ode_result] (const Tvec& state, const double t) {
          ode_result.push_back(t, state);
          std::cout << "\rTime:" << std::setw(8) << t << std::flush;
        });
    std::cout << std::endl;

    std::cout << std::resetiosflags(std::ios::fixed);
    std::cout << std::setprecision(saved_precision);

    return ode_result;
  }

}; // class ODEIntegrator


} // namespace KAF


