#include <iostream> // ostream
#include <string>
#include <vector>
#include <array>
#include <cmath> // sin, cos
#include <random> // default_random_engine, uniform_real_distribution
#include <iomanip> // setprecision

#include <libconfig.hh>

#include <KAF/Containers.hpp>
#include <KAF/ODE.hpp>
#include <KAF/Auxiliary.hpp>


typedef std::array<double, 2> t_oscillator_state;
typedef std::array<double, 3> t_lorenz63_state;
typedef std::vector<double>   t_lorenz96_state;
typedef std::array<double, 4> t_doublewell_state;

KAF::TimeSeries run_lorenz63(
    const KAF::ODEIntegratorParameters& integrator_parameters,
    const t_lorenz63_state lorenz63_ic,
    const double rho)
{
  using namespace KAF;

  const std::string PREFIX = "• ";

  std::cout << PREFIX << "Running Lorenz '63" << std::endl;
  Timer timer;

  const std::vector<std::string> parameter_names  = { "sigma", "rho", "beta" };
  const std::vector<double>      parameter_values = {      10,  rho ,   8./3 };

  const ODEParameters lorenz63_parameters(
      parameter_names.begin(),
      parameter_values.begin(),
      3);
  std::cout << lorenz63_parameters << std::endl;

  ODEIntegrator integrator(integrator_parameters);

  TimeSeries lorenz63_result = integrator.run(
      lorenz63_ic,
      Lorenz63<t_lorenz63_state>(),
      lorenz63_parameters);

  std::cout << PREFIX << "Lorenz '63 integration: " << timer << std::endl;
  std::cout << PREFIX << "Timesteps: " << lorenz63_result.get_timesteps();
  std::cout << std::endl << std::endl;

  return lorenz63_result;
}

KAF::TimeSeries run_lorenz96(
    const KAF::ODEIntegratorParameters& integrator_parameters,
    const t_lorenz96_state lorenz96_ic,
    const unsigned K,
    const unsigned J)
{
  using namespace KAF;

  const std::string PREFIX = "• ";

  std::cout << PREFIX << "Running Lorenz '96" << std::endl;
  Timer timer;

  const std::vector<std::string> parameter_names  = { "F", "hx", "hy", "eps" };
  const std::vector<double>      parameter_values = {10.0, -0.8,  1.0,  1./128};

  const ODEParameters lorenz96_parameters(
      parameter_names.begin(),
      parameter_values.begin(),
      4);
  std::cout << lorenz96_parameters << std::endl;

  ODEIntegrator integrator(integrator_parameters);

  TimeSeries lorenz96_result = integrator.run(
      lorenz96_ic,
      Lorenz96<t_lorenz96_state>(K, J),
      lorenz96_parameters);

  std::cout << PREFIX << "Lorenz '96 integration: " << timer << std::endl;
  std::cout << PREFIX << "Timesteps: " << lorenz96_result.get_timesteps();
  std::cout << std::endl << std::endl;

  return lorenz96_result;
}

KAF::TimeSeries run_doublewell(
    const KAF::ODEIntegratorParameters& integrator_parameters,
    const t_doublewell_state doublewell_ic,
    const double epsilon = 1e-2,
    const double forcing = 4./90)
{
  using namespace KAF;

  const std::string PREFIX = "• ";

  std::cout << PREFIX << "Running double-well" << std::endl;
  Timer timer;

  const std::vector<std::string> parameter_names  =
    { "sigma", "rho", "beta", "epsilon", "forcing" };
  const std::vector<double>      parameter_values =
    {      10,    28,   8./3,  epsilon ,  forcing  };

  const ODEParameters doublewell_parameters(
      parameter_names.begin(),
      parameter_values.begin(),
      parameter_names.size());
  std::cout << doublewell_parameters << std::endl;

  ODEIntegrator integrator(integrator_parameters);

  TimeSeries doublewell_result = integrator.run(
      doublewell_ic,
      DoubleWell<t_doublewell_state>(),
      doublewell_parameters);

  std::cout << PREFIX << "Double-well integration: " << timer << std::endl;
  std::cout << PREFIX << "Timesteps: " << doublewell_result.get_timesteps();
  std::cout << std::endl << std::endl;;

  return doublewell_result;
}

KAF::TimeSeries generate_harmonic_oscillator(
    const KAF::ODEIntegratorParameters& integrator_parameters,
    const double angular_speed = 1.0)
{
  const auto n_approximately_steps =
    integrator_parameters.get_approximately_steps_count();
  const auto dt = integrator_parameters.dt;
  const auto t0 = integrator_parameters.t0;

  KAF::TimeSeries oscillator(2, n_approximately_steps);
  for (size_t k = 0; k < n_approximately_steps; ++k) {
    const double t = k * dt + t0;
    oscillator.push_back<t_oscillator_state>(
        t, { std::cos(angular_speed * t), std::sin(angular_speed * t) });
  }

  return oscillator;
}

t_lorenz96_state generate_random_L96_state(unsigned K, unsigned J, unsigned seed)
{
  std::default_random_engine dre(seed);
  std::uniform_real_distribution<double> slow_distribution(-3.0, 10.0);
  std::uniform_real_distribution<double> fast_distribution(-5.0, 12.5);

  t_lorenz96_state state(J*K + K);
  for (unsigned k = 0; k < K; ++k) {
    state[k] = slow_distribution(dre);
  }
  for (unsigned jk = K; jk < K + J*K; ++jk) {
    state[jk] = fast_distribution(dre);
  }

  return state;
}

void output_L96_state(
    std::ostream& out,
    unsigned K,
    unsigned J,
    const t_lorenz96_state& state)
{
    const auto saved_precision = out.precision();

    out << std::setprecision(4);
    out << std::left << std::setw(20) << "x_k variables:";
    for (unsigned k = 0; k < K; ++k) {
      out << std::setw(11) << state[k];
    }
    out << std::endl;

    out << std::left << std::setw(20) << "y_{j,k} variables:";
    for (unsigned j = 0; j < J; ++j) {
      for (unsigned k = 0; k < K; ++k) {
        out << std::setw(11) << state[K + j + J*k];
      }
      out << std::endl << std::setw(20) << " ";
    }
    out << std::endl;

    out << std::setprecision(saved_precision);
}



int main(int argc, const char** argv)
{
  libconfig::Config config;
  config.readFile("generate_data.cfg");

  const bool RUN_LORENZ_63  = config.lookup("control-flow.RUN_LORENZ_63");
  const bool RUN_LORENZ_96  = config.lookup("control-flow.RUN_LORENZ_96");
  const bool RUN_DOUBLEWELL = config.lookup("control-flow.RUN_DOUBLEWELL");
  const bool GENERATE_HARMONIC_OSCILLATOR =
    config.lookup("control-flow.GENERATE_HARMONIC_OSCILLATOR");

  const bool generate_smth =
    RUN_LORENZ_63 ||
    RUN_LORENZ_96 ||
    RUN_DOUBLEWELL ||
    GENERATE_HARMONIC_OSCILLATOR;
  if (!generate_smth) {
    std::cerr << "No generation data chosen; exiting" << std::endl;
    return 1;
  }

  const double T0             = config.lookup("ode-integrator.T0");
  const double T1             = config.lookup("ode-integrator.T1");
  const double T1_CONVERGENCE = config.lookup("ode-integrator.T1_CONVERGENCE");
  const double DT             = config.lookup("ode-integrator.DT");
  const double ABSTOL         = config.lookup("ode-integrator.ABSTOL");
  const double RELTOL         = config.lookup("ode-integrator.RELTOL");

  const KAF::ODEIntegratorParameters integrator_parameters(
      T0, T1, DT, ABSTOL, RELTOL);
  const KAF::ODEIntegratorParameters integrator_parameters_convergence(
      T0, T1_CONVERGENCE, DT, ABSTOL, RELTOL);

  KAF::NumpyReaderWriter numpy;

  if (RUN_LORENZ_63) {
    const libconfig::Setting& ic_setting = config.lookup("lorenz-63.ic");
    const t_lorenz63_state ic = {
      ic_setting[0],
      ic_setting[1],
      ic_setting[2]
    };

    const libconfig::Setting& RHO_ARRAY = config.lookup("lorenz-63.RHO_ARRAY");

    KAF::TimeSeriesBunch lorenz63_bunch;
    for (auto&& rho : RHO_ARRAY) {
      auto&& lorenz63_convergence = run_lorenz63(
          integrator_parameters_convergence, ic, rho);

      const size_t n_convergence_timesteps = lorenz63_convergence.size();
      const double* last_vector =
        lorenz63_convergence.get_vector_pointer(n_convergence_timesteps - 1);

      const t_lorenz63_state ic_after_convergence = {
        last_vector[0],
        last_vector[1],
        last_vector[2]
      };

      std::cerr << "ic_after_convergence: "
        << ic_after_convergence[0] << ", "
        << ic_after_convergence[1] << ", "
        << ic_after_convergence[2] << std::endl;

      auto&& lorenz63_result = run_lorenz63(
          integrator_parameters, ic_after_convergence, rho);

      lorenz63_bunch.push_back(std::move(lorenz63_result));
    }

    numpy.write("lorenz63.npz", lorenz63_bunch);
  }

  if (RUN_LORENZ_96) {
    const unsigned K = config.lookup("lorenz-96.K");
    const unsigned J = config.lookup("lorenz-96.J");
    const unsigned IC_SEED = config.lookup("lorenz-96.IC_SEED");
    const t_lorenz96_state ic = generate_random_L96_state(K, J, IC_SEED);

    auto&& lorenz96_convergence = run_lorenz96(
        integrator_parameters_convergence, ic, K, J);

    const size_t n_convergence_timesteps = lorenz96_convergence.size();
    const double* last_vector =
      lorenz96_convergence.get_vector_pointer(n_convergence_timesteps - 1);

    const t_lorenz96_state ic_after_convergence(
        last_vector, last_vector + ic.size());

    std::cerr << "ic_after_convergence: " << std::endl;
    output_L96_state(std::cerr, K, J, ic_after_convergence);

    auto&& lorenz63_result = run_lorenz96(
        integrator_parameters, ic_after_convergence, K, J);

    numpy.write("lorenz96_trajectory.npy", lorenz63_result);
    numpy.write("lorenz96_times.npy", lorenz63_result.get_time_array());
  }

  if (RUN_DOUBLEWELL) {
    const double EPS     = config.lookup("double-well.EPS");
    const double FORCING = config.lookup("double-well.FORCING");
    const libconfig::Setting& ic_setting = config.lookup("double-well.ic");
    const t_doublewell_state ic = {
      ic_setting[0],
      ic_setting[1],
      ic_setting[2],
      ic_setting[3]
    };

    const auto doublewell_result = run_doublewell(
        integrator_parameters, ic, EPS, FORCING);

    const auto doublewell_x = doublewell_result.coordinate_slice(0);

    numpy.write("doublewell_trajectory.npy", doublewell_x);
    numpy.write("doublewell_times.npy", doublewell_result.get_time_array());
  }

  if (GENERATE_HARMONIC_OSCILLATOR) {
    const libconfig::Setting& ANGULAR_SPEED_ARRAY =
      config.lookup("harmonic-oscillator.ANGULAR_SPEED_ARRAY");

    KAF::TimeSeriesBunch oscillator_bunch;
    for (auto&& angular_speed : ANGULAR_SPEED_ARRAY) {
      auto&& oscillator = generate_harmonic_oscillator(
          integrator_parameters, angular_speed);

      oscillator_bunch.push_back(std::move(oscillator));
    }

    numpy.write("oscillator.npz", oscillator_bunch);
  }

  return 0;
}


