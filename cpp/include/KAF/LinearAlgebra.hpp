#pragma once

#include <cmath>
#include <utility>
#include <memory>
#include <algorithm>
#include <stdexcept>

#include <arpackpp/ardsmat.h>    // ARdsSymMatrix
#include <arpackpp/ardssym.h>    // ARluSymStdEig
#include <arpackpp/ardnsmat.h>   // ARdsNonSymMatrix
#include <arpackpp/arssym.h>     // ARSymStdEig
#include <arpackpp/blas1c.h>     // scal

#include <Eigen/Core>

#include <KAF/PairDistUtils.hpp>
#include <KAF/Utilities.hpp>
#include <KAF/Containers.hpp>
#include <KAF/Auxiliary.hpp>


namespace KAF {

class EigenDecomposition {
  /*
   * Store an eigen decomposition
   */
public:
  typedef Eigen::VectorXd Values;
  typedef Eigen::MatrixXd Vectors;

private:
  static constexpr size_t DIRECT_OUTPUT_LIMIT = 50;
  static constexpr size_t N_CONDENSED_OUTPUT  = 5;

  Values  values;
  Vectors vectors;
  bool initialized = false;

public:
  EigenDecomposition() {}

  EigenDecomposition(EigenDecomposition&& source_ed) :
    values(std::move(source_ed.values)),
    vectors(std::move(source_ed.vectors)),
    initialized(true) {}

  EigenDecomposition(const EigenDecomposition& source_ed) :
    values(source_ed.values),
    vectors(source_ed.vectors),
    initialized(true) {}

  EigenDecomposition(Values&& values, Vectors&& vectors) :
    values(std::move(values)),
    vectors(std::move(vectors)),
    initialized(true) {}

  EigenDecomposition& operator=(EigenDecomposition&& source_ed)
  {
    values.swap(source_ed.values);
    vectors.swap(source_ed.vectors);

    initialized = true;

    return *this;
  }

  const Values& get_values() const
  {
    return values;
  }

  const Vectors& get_vectors() const
  {
    return vectors;
  }

  Vectors get_vectors(const double normalization) const
  {
    return vectors * normalization;
  }

  friend std::ostream&
    operator<<(std::ostream& out, const EigenDecomposition& ed)
  {
    out << "Values:" << std::endl;
    if (ed.values.size() < DIRECT_OUTPUT_LIMIT) {
      out << ed.values << std::endl;

      out << "Vectors:" << std::endl;
      if (ed.vectors.rows() < DIRECT_OUTPUT_LIMIT) {
        out << ed.vectors << std::endl;
      } else {
        out << ed.vectors.topRows(N_CONDENSED_OUTPUT) << std::endl;
        out << "..." << std::endl;
        out << ed.vectors.bottomRows(N_CONDENSED_OUTPUT) << std::endl;
      }
    } else {
      out << ed.values.head(N_CONDENSED_OUTPUT) << std::endl;
      out << '.' << std::endl << '.' << std::endl << '.' << std::endl;
      out << ed.values.tail(N_CONDENSED_OUTPUT) << std::endl;

      out << "Vectors:" << std::endl;
      // since this is eigenvalue decomposition, dimension >= no of eigenvalues
      out << ed.vectors.topLeftCorner(N_CONDENSED_OUTPUT, N_CONDENSED_OUTPUT)
        << "..."
        << ed.vectors.topRightCorner(N_CONDENSED_OUTPUT, N_CONDENSED_OUTPUT)
        << std::endl;
      out << "..." << std::endl;
      out << ed.vectors.bottomLeftCorner(N_CONDENSED_OUTPUT, N_CONDENSED_OUTPUT)
        << "..."
        << ed.vectors.bottomRightCorner(N_CONDENSED_OUTPUT, N_CONDENSED_OUTPUT)
        << std::endl;
    }

    return out;
  }

}; // class EigenDecomposition


class SingularValueDecomposition {
public:
  typedef Eigen::VectorXd Values;
  typedef Eigen::MatrixXd Vectors;

private:
  Values         values;
  Vectors  left_vectors;
  Vectors right_vectors;
  bool initialized = false;

  static void normalize_vectors(Vectors& vectors, const double normalization)
  {
    for (unsigned k = 0; k < vectors.cols(); ++k) {
      vectors.col(k).normalize();
    }
    vectors *= normalization;
  }

public:
  SingularValueDecomposition() {}

  SingularValueDecomposition(SingularValueDecomposition&& source_svd) :
    values(std::move(source_svd.values)),
    left_vectors(std::move(source_svd.left_vectors)),
    right_vectors(std::move(source_svd.right_vectors)),
    initialized(true) {}

  SingularValueDecomposition(
      Values&& values,
      Vectors&& left_vectors,
      Vectors&& right_vectors) :
    values(std::move(values)),
    left_vectors(std::move(left_vectors)),
    right_vectors(std::move(right_vectors)),
    initialized(true) {}

  SingularValueDecomposition& operator=(SingularValueDecomposition&& source_svd)
  {
    values.swap(source_svd.values);
    left_vectors.swap(source_svd.left_vectors);
    right_vectors.swap(source_svd.right_vectors);

    initialized = true;

    return *this;
  }

  size_t get_count() const
  {
    return values.size();
  }

  const Values& get_values() const
  {
    return values;
  }

  Eigen::ArrayXd get_squared_values() const
  {
    return values.array().square();
  }

  const Vectors& get_left_vectors() const
  {
    return left_vectors;
  }

  const Vectors& get_right_vectors() const
  {
    return right_vectors;
  }

  void normalize_left_vectors(const double normalization = 1.0)
  {
    normalize_vectors(left_vectors, normalization);
  }

  void normalize_right_vectors(const double normalization = 1.0)
  {
    normalize_vectors(right_vectors, normalization);
  }

}; // class SingularValueDecomposition


class OMPReductions {
public:
    #pragma omp declare reduction \
        (merge :\
         std::vector<Eigen::Triplet<double> > :\
         omp_out.insert(omp_out.end(), omp_in.begin(), omp_in.end()))

}; // class OMPReductions


class KernelEvaluator {
  /*
   * In methods of this class, typically,
   *    Data = { PairwiseDistances | TimeSeries },
   * but can be anything as long as 'get_point_count' method is provided, and
   * kernel has a method 'evaluate' that takes it as a parameter.
   */
private:
  class KernelConcept;
  template <typename Kernel> class KernelModel;

  std::unique_ptr<KernelConcept>     p_kernel;
  std::unique_ptr<PairDistAssembler> p_pairdist_assembler;

  /* private methods **********************************************************/
  double compute_sparsity_threshold(
      const PairwiseDistances& pairwise_dces,
      const double sparsity_threshold_coeff) const
  {
    const auto distance_min  = pairwise_dces.minCoeff();
    const auto distance_max  = pairwise_dces.maxCoeff();

    // swap because monotonically decreasing
    const auto kernel_value_min  = p_kernel->evaluate(distance_max);
    const auto kernel_value_max  = p_kernel->evaluate(distance_min);

    const auto sparsity_threshold =
      sparsity_threshold_coeff * (kernel_value_max - kernel_value_min)
      + kernel_value_min;

    /*
    std::cout << "Distance minimum: " << distance_min  << std::endl;
    std::cout << "Distance maximum: " << distance_max  << std::endl;
    std::cout << "Kernel value minimum: " << kernel_value_min  << std::endl;
    std::cout << "Kernel value maximum: " << kernel_value_max  << std::endl;
    std::cerr << "Sparsity threshold: " << sparsity_threshold  << std::endl;
    */

    return sparsity_threshold;
  }

public:
  template <typename Kernel>
    KernelEvaluator(Kernel kernel) :
      p_kernel(std::make_unique<KernelModel<Kernel> >(std::move(kernel))) {}

  template <typename Kernel>
    KernelEvaluator(Kernel kernel, PairDistAssembler assembler) :
      p_kernel(
          std::make_unique<KernelModel<Kernel> >(std::move(kernel))),
      p_pairdist_assembler(
          std::make_unique<PairDistAssembler>(std::move(assembler))) {}

  DenseMatrix compute_dense_matrix(PairDistFetcher&& fetcher) const
  {
    if (!p_pairdist_assembler) {
      throw std::logic_error(
          "[KernelEvaluator::compute_dense_matrix(PairDistFetcher&&)] "
          "p_pairdist_assembler is not set");
    }
    auto&& pair_distances = (*p_pairdist_assembler)(std::move(fetcher));
    return compute_dense_matrix(pair_distances);
  }

  SparseMatrix compute_sparse_matrix(
      PairDistFetcher&& fetcher,
      const double sparsity_threshold_coeff) const
  {
    if (!p_pairdist_assembler) {
      throw std::logic_error(
          "[KernelEvaluator::compute_sparse_matrix(PairDistFetcher&&, const double)] "
          "p_pairdist_assembler is not set");
    }
    auto&& pair_distances = (*p_pairdist_assembler)(std::move(fetcher));
    return compute_sparse_matrix(pair_distances, sparsity_threshold_coeff);
  }

  DenseMatrix compute_dense_matrix(const PairwiseDistances& pairwise_dces) const
  {
    const auto n_vertical_points   = pairwise_dces.get_vertical_point_count();
    const auto n_horizontal_points = pairwise_dces.get_horizontal_point_count();

    DenseMatrix kernel_matrix(n_vertical_points, n_horizontal_points);

    if (pairwise_dces.is_symmetric()) {
      const auto& n_points = n_vertical_points;

      #pragma omp parallel for
      for (size_t j = 0; j < n_points; ++j) {
        for (size_t i = j + 1; i < n_points; ++i) {
          const auto value = p_kernel->evaluate(pairwise_dces(i,j));
          kernel_matrix.coeffRef(i,j) = value;
          kernel_matrix.coeffRef(j,i) = value; // XXX delete to save space
        }
      }
      for (size_t i = 0; i < n_points; ++i) {
        kernel_matrix.coeffRef(i,i) = p_kernel->evaluate(pairwise_dces(i,i));
      }
    } else {
      #pragma omp parallel for
      for (size_t j = 0; j < n_horizontal_points; ++j) {
        for (size_t i = 0; i < n_vertical_points; ++i) {
          const auto value = p_kernel->evaluate(pairwise_dces(i,j));
          kernel_matrix.coeffRef(i,j) = value;
        }
      }
    }

    return kernel_matrix;
  }

  SparseMatrix compute_sparse_matrix(
      const PairwiseDistances& pairwise_dces,
      const double sparsity_threshold_coeff) const
  {
    if (sparsity_threshold_coeff <= 0.0) {
      throw std::logic_error(
          "[KernelEvaluator::compute_sparse_matrix] "
          "sparsity_threshold_coeff parameter cannot be <= 0.0");
    }

    const auto sparsity_threshold = compute_sparsity_threshold(
        pairwise_dces, sparsity_threshold_coeff);

    const auto n_vertical_points   = pairwise_dces.get_vertical_point_count();
    const auto n_horizontal_points = pairwise_dces.get_horizontal_point_count();
    const size_t n_estimated_neighbors = 0.1 * n_vertical_points;

    std::vector<Eigen::Triplet<double> > triplets;
    triplets.reserve(n_estimated_neighbors * n_horizontal_points);

    if (pairwise_dces.is_symmetric()) {
      const auto& n_points = n_vertical_points;

      #pragma omp parallel for reduction(OMPReductions::merge: triplets)
      for (size_t j = 0; j < n_points; ++j) {
        for (size_t i = j + 1; i < n_points; ++i) {
          const auto value = p_kernel->evaluate(pairwise_dces(i,j));
          if (value >= sparsity_threshold) {
            triplets.emplace_back(i, j, value);
            triplets.emplace_back(j, i, value); // XXX delete to save space
          }
        }
      }
      for (size_t i = 0; i < n_points; ++i) {
        const auto value = p_kernel->evaluate(pairwise_dces(i,i));
        if (value >= sparsity_threshold) {
          triplets.emplace_back(i, i, value);
        }
      }

    } else {
      #pragma omp parallel for reduction(OMPReductions::merge: triplets)
      for (size_t j = 0; j < n_horizontal_points; ++j) {
        for (size_t i = 0; i < n_vertical_points; ++i) {
          const auto value = p_kernel->evaluate(pairwise_dces(i,j));
          if (value >= sparsity_threshold) {
            triplets.emplace_back(i, j, value);
          }
        }
      }
    }
    SparseMatrix kernel_matrix(n_vertical_points, n_horizontal_points);
    kernel_matrix.setFromTriplets(triplets.begin(), triplets.end());

    return kernel_matrix;
  }


private:
  class KernelConcept {
    public:
      virtual ~KernelConcept() = default;
      virtual double evaluate(const double x) const = 0;
  }; // class KernelConcept

  template <typename Kernel>
  class KernelModel : public KernelConcept {
    private:
      Kernel kernel;

    public:
      KernelModel(Kernel kernel) :
        kernel(std::move(kernel)) {}

      double evaluate(const double x) const final override
      {
        return kernel.evaluate(x);
      }
  }; // class KernelModel

}; // class KernelEvaluator


class MatrixToolbox {
private:
  template<typename Matrix>
  static std::unique_ptr<double[]> allocate_memory_to_store(
      const Matrix& matrix)
  {
    const size_t n_cols = matrix.cols();
    const size_t n_rows = matrix.rows();
    const size_t n_values = n_cols * n_rows;
    auto raw_matrix = std::make_unique<double[]>(n_values);

    return raw_matrix;
  }

  static std::unique_ptr<double[]> convert_eigen_to_raw(
      const DenseMatrix& matrix)
  {
    auto raw_matrix = allocate_memory_to_store(matrix);

    const size_t n_cols = matrix.cols();
    const size_t n_rows = matrix.rows();

    for (size_t j = 0; j < n_cols; ++j) {
      for (size_t i = 0; i < n_rows; ++i) {
        raw_matrix[i + n_rows * j] = matrix.coeff(i,j);
      }
    }

    return raw_matrix;
  }

  static std::unique_ptr<double[]> convert_eigen_to_raw(
      const SparseMatrix& matrix)
  {
    if (matrix.IsRowMajor) {
      throw std::logic_error(
          "[MatrixToolbox::convert_eigen_to_raw(const SparseMatrix&)] "
          "matrix is not column-major, cannot convert");
    }

    auto raw_matrix = allocate_memory_to_store(matrix);

    const size_t n_cols = matrix.cols();
    const size_t n_rows = matrix.rows();
    const size_t n_values = n_cols * n_rows;

    for (size_t i = 0; i < n_values; ++i) {
      raw_matrix[i] = 0.0;
    }

    for (size_t j = 0; j < matrix.outerSize(); ++j) {
      for (SparseMatrix::InnerIterator it(matrix, j); it; ++it) {
        const auto& i = it.row();
        raw_matrix[i + n_rows * j] = it.value();
      }
    }

    return raw_matrix;
  }

public:
  MatrixToolbox() {}

  static Eigen::ArrayXd compute_column_sums(const DenseMatrix& matrix)
  {
    return matrix.colwise().sum();
  }

  static Eigen::ArrayXd compute_column_sums(const SparseMatrix& matrix)
  {
    const auto n_rows = matrix.rows();
    return Eigen::VectorXd::Ones(n_rows).transpose() * matrix;
  }

  static Eigen::ArrayXd compute_row_sums(const DenseMatrix& matrix)
  {
    return matrix.rowwise().sum();
  }

  static Eigen::ArrayXd compute_row_sums(const SparseMatrix& matrix)
  {
    const auto n_cols = matrix.cols();
    return matrix * Eigen::VectorXd::Ones(n_cols);
  }

  template<typename Matrix>
  static EigenDecomposition compute_ed_symmetric(
      const Matrix& matrix,
      const size_t n_eig)
  {
    if (matrix.cols() != matrix.rows()) {
      throw std::logic_error(
          "[MatrixToolbox::compute_ed_symmetric(const Matrix&, const size_t)] "
          "matrix is not square, cannot compute eigen decomposition");
    }

    auto raw_matrix = convert_eigen_to_raw(matrix);
    const size_t d_matrix = matrix.cols();

    typedef ARdsSymMatrix<double> ArMatrix;
    ArMatrix ar_matrix(d_matrix, raw_matrix.get());
    ARluSymStdEig<double> eigen_solver(n_eig, ar_matrix, "LA");

    double* raw_values  = NULL;
    double* raw_vectors = NULL;

    eigen_solver.FindEigenvectors();
    eigen_solver.EigenValVectors(raw_vectors, raw_values);

    EigenDecomposition::Values   values = Eigen::Map<Eigen::VectorXd>(
        raw_values, n_eig);
    EigenDecomposition::Vectors vectors = Eigen::Map<Eigen::MatrixXd>(
        raw_vectors, d_matrix, n_eig);

    delete[] raw_values;
    delete[] raw_vectors;

    for (size_t k = 0; k < n_eig; ++k) {
      vectors.col(k).normalize();
    }

    return EigenDecomposition(
        std::move(values),
        std::move(vectors));
  }

  template<typename Matrix>
  static SingularValueDecomposition compute_svd(
      const Matrix& matrix,
      const size_t n_sing)
  {
    auto raw_matrix = convert_eigen_to_raw(matrix);

    const size_t n_cols = matrix.cols();
    const size_t n_rows = matrix.rows();

    typedef ARdsNonSymMatrix<double, double> ArMatrix;
    ArMatrix ar_matrix(n_rows, n_cols, raw_matrix.get());

    ARSymStdEig<double, ArMatrix>
      right_eigen_solver(n_cols, n_sing, &ar_matrix, &ArMatrix::MultMtMv);

    double* raw_values        = NULL;
    double* raw_right_vectors = NULL;

    right_eigen_solver.EigenValVectors(raw_right_vectors, raw_values);

    Eigen::VectorXd values        = Eigen::Map<Eigen::VectorXd>(
        raw_values, n_sing);
    Eigen::MatrixXd right_vectors = Eigen::Map<Eigen::MatrixXd>(
        raw_right_vectors, n_cols, n_sing);

    delete[] raw_values;
    delete[] raw_right_vectors;

    values.reverseInPlace();
    right_vectors.rowwise().reverseInPlace(); // this is actually reversing columns

    values = values.cwiseSqrt();

    // computing left vectors from the right ones and singular values
    SingularValueDecomposition::Vectors left_vectors(n_rows, n_sing);
    for (size_t k = 0; k < n_sing; ++k) {
      ar_matrix.MultMv(
          right_vectors.col(k).data(),   // src
          left_vectors.col(k).data());   // dest
      auto sigma_inv = 1.0 / values[k];
      scal(n_rows, sigma_inv, left_vectors.col(k).data(), 1);
    }

    return SingularValueDecomposition(
        std::move(values),
        std::move(left_vectors),
        std::move(right_vectors));
  }

}; // class MatrixToolbox

} // namespace KAF


