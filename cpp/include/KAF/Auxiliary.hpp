#pragma once

#include <ostream>
#include <iomanip> // setw
#include <chrono>
#include <string> // string, to_string
#include <vector>
#include <stdexcept>
#include <filesystem> // path, exists, create_directory, filesystem_error

#include <Eigen/Core>
#include <Eigen/SparseCore>

#include <cnpy.h>

#include <KAF/Containers.hpp>
#include <KAF/ODE.hpp>


namespace KAF {

class Timer {
private:
  typedef std::chrono::high_resolution_clock::time_point time_point;

  time_point start;
  static constexpr double MICRO = 1000000;

  static time_point get_now()
  {
    return std::chrono::high_resolution_clock::now();
  }

public:
  Timer() :
    start(get_now()) {}

  void restart()
  {
    start = get_now();
  }

  double get_seconds() const
  {
    using namespace std::chrono;
    return duration_cast<microseconds>(get_now() - start).count() / MICRO;
  }

  friend std::ostream& operator<<(std::ostream& out, const Timer& timer)
  {
    out << timer.get_seconds() << " s";

    return out;
  }

}; // class Timer


class Printer {
private:
  std::ostream& out;
  const std::string prefix;
  unsigned header_width;

public:
  template <typename String>
  Printer(std::ostream& out, String&& prefix, unsigned header_width = 0) :
      out(out), prefix(prefix), header_width(header_width) {}

  template <typename Header, typename... Args>
  std::ostream& operator()(Header&& header, Args&&... args)
  {
    out << prefix;
    if (header_width > 0) {
      out << std::setw(header_width) << std::left;
    }
    out << header;
    (out << ... << args);
    out << std::endl;
    return out;
  }

  template <typename Outputtable>
  std::ostream& operator<<(Outputtable&& outputtable)
  {
    out << prefix;
    out << outputtable;
    return out;
  }

  std::ostream& raw()
  {
    return out;
  }

  void newline()
  {
    out << std::endl;
  }

  void set_header_width(unsigned new_width)
  {
    header_width = new_width;
  }

}; // class Printer

Printer info(std::cout, "• ");
Printer debug(std::cerr, "~ ");
Printer warn(std::cerr, "! ");
Printer error(std::cerr, " =====----o----=====\n =====--ERROR--=====\n ");


class NumpyReaderWriter {
private:
  const std::string input_prefix;
  const std::string output_prefix;

public:
  NumpyReaderWriter() {}

  NumpyReaderWriter(
      const std::string& input_prefix,
      const std::string& output_prefix) :
    input_prefix(input_prefix), output_prefix(output_prefix)
  {
    namespace fs = std::filesystem;
    const fs::path  input_path {  input_prefix };
    const fs::path output_path { output_prefix };

    try {
      if (!fs::exists(input_path) && input_path != output_path) {
        throw std::runtime_error(
            "[NumpyReaderWriter(const std::string&, const std::string&)]\n"
            "input path does not exist");
      }

      if (!fs::exists(output_path)) {
        warn << "[NumpyReaderWriter(const std::string&, const std::string&)]\n";
        warn << "output path does not exist, attempting to create:\n";
        warn << output_path << std::endl;
        fs::create_directory(output_path);
      }
    } catch (const fs::filesystem_error& e) {
      error("[NumpyReaderWriter(const std::string&, const std::string&)]\n",
          e.what());
      throw;
    }
  }

  void write(const std::string& file_name, const std::vector<double>& vec) const
  {
    cnpy::npy_save(output_prefix + file_name, vec.data(), {vec.size()}, "w");
  }

  void write(const std::string& file_name, const TimeSeries& timeseries) const
  {
    const size_t d_vector = timeseries.get_vector_dimension();

    if (d_vector == 1) {
      cnpy::npy_save(
          output_prefix + file_name,
          timeseries.get_data_pointer(),
          {timeseries.get_timesteps()},
          "w");
    } else {
      cnpy::npy_save(
          output_prefix + file_name,
          timeseries.get_data_pointer(),
          {timeseries.get_timesteps(), d_vector},
          "w");
    }
  }

  void write(const std::string& file_name, const TimeSeriesBunch& ts_bunch) const
  {
    if (ts_bunch.empty())
    {
      std::cerr << "[write(const std::string&, const TimeSeriesBunch&)] " <<
        "TimeSeriesBunch is empty, nothing to write" << std::endl;
      return;
    }

    const auto full_file_path = output_prefix + file_name;

    auto write_ts = [&full_file_path](
        const std::string& array_name,
        const TimeSeries& timeseries,
        const std::string& mode)
    {
      const size_t d_vector = timeseries.get_vector_dimension();

      if (d_vector == 1) {
        cnpy::npz_save(
            full_file_path,
            array_name,
            timeseries.get_data_pointer(),
            {timeseries.get_timesteps()},
            mode);
      } else {
        cnpy::npz_save(
            full_file_path,
            array_name,
            timeseries.get_data_pointer(),
            {timeseries.get_timesteps(), d_vector},
            mode);
      }
    };

    auto write_vec = [&full_file_path](
        const std::string& array_name,
        const std::vector<double>& vector,
        const std::string& mode)
    {
      const size_t d_vector = vector.size();
      cnpy::npz_save(
          full_file_path,
          array_name,
          vector.data(),
          {vector.size()},
          mode);
    };

    write_ts("series_0", ts_bunch[0],                  "w");
    write_vec("time_0",  ts_bunch[0].get_time_array(), "a");

    for (size_t k = 1; k < ts_bunch.size(); ++k) {
      write_ts("series_"+ std::to_string(k), ts_bunch[k],                  "a");
      write_vec("time_" + std::to_string(k), ts_bunch[k].get_time_array(), "a");
    }
  }

  void write(const std::string& file_name, const Eigen::MatrixXd& matrix) const
  {
    const size_t n_rows = matrix.rows();
    const size_t n_cols = matrix.cols();

    cnpy::npy_save(
        output_prefix + file_name, matrix.data(), {n_cols, n_rows}, "w");
  }

  void write(
      const std::string& file_name,
      const Eigen::SparseMatrix<double>& matrix) const
  {
    write(file_name, Eigen::MatrixXd(matrix));
  }

  void write(const std::string& file_name, const Eigen::VectorXd& vector) const
  {
    const size_t d_vector = vector.size();

    cnpy::npy_save(output_prefix + file_name, vector.data(), {d_vector}, "w");
  }

  TimeSeriesBunch read_ts_bunch(const std::string& file_name) const
  {
    const auto npz_file = cnpy::npz_load(input_prefix + file_name);
    TimeSeriesBunch ts_bunch;
    const std::string series_ = { "series_" };
    const std::string time_   = { "time_"   };

    for (auto&& [array_name, npy_array] : npz_file) {
      const auto d_array = npy_array.shape.size();

      if (d_array == 0 || 2 < d_array) {
        std::cerr << "[read_ts_bunch] dimension of array " << array_name.c_str()
                  << " is 0 or >2; skipping" << std::endl;
        continue;
      }

      if (array_name.find(series_) != std::string::npos) { // check if array_name contains "series_"
        const auto d_vector = [&npy_array, &d_array]() -> size_t {
          if (d_array == 2) {
            return npy_array.shape[1];
          } else {
            return 1;
          }
        } ();

        const auto array_subname = array_name.substr(7);
        const auto npy_time = npz_file.at(time_ + array_subname);

        ts_bunch.emplace_back(
            npy_array.as_vec<double>(), npy_time.as_vec<double>(), d_vector);
      }
    }

    return ts_bunch;
  }

  TimeSeries read_timeseries(
      const std::string& series_name,
      const std::string& time_name) const
  {
    const auto npy_series = cnpy::npy_load(input_prefix + series_name);
    const auto npy_time   = cnpy::npy_load(input_prefix + time_name);

    const auto d_series = npy_series.shape.size();
    if (d_series != 1 && d_series != 2) {
      throw std::runtime_error("[read_timeseries] "
          "dimension of series array is not 1 or 2");
    }

    const auto d_time = npy_time.shape.size();
    if (d_time != 1) {
      throw std::runtime_error("[read_timeseries] "
          "dimension of time array is not 1");
    }

    const auto d_vector = [&npy_series, &d_series]() -> size_t {
      if (d_series == 2) {
        return npy_series.shape[1];
      } else {
        return 1;
      }
    } ();

    return {npy_series.as_vec<double>(), npy_time.as_vec<double>(), d_vector};
  }

  Series read_series(const std::string& file_name) const
  {
    const auto npy_array = cnpy::npy_load(input_prefix + file_name);

    const auto d_array = npy_array.shape.size();
    if (d_array != 1 && d_array != 2) {
      throw std::runtime_error("[read_series] array dimension is not 1 or 2");
    }

    const auto d_vector = [&npy_array, &d_array]() -> size_t {
      if (d_array == 2) {
        return npy_array.shape[1];
      } else {
        return npy_array.shape[0];
      }
    } ();

    return {npy_array.as_vec<double>(), d_vector};
  }

  Eigen::MatrixXd read_matrix(const std::string& file_name) const
  {
    const auto npy_array = cnpy::npy_load(input_prefix + file_name);

    const auto d_array = npy_array.shape.size();
    if (d_array != 2) {
      throw std::runtime_error("[read_matrix] array dimension is not 2");
    }

    const size_t n_cols = npy_array.shape[0]; // swapped because cnpy also
    const size_t n_rows = npy_array.shape[1]; // writes in Fortran order

    return Eigen::Map<const Eigen::MatrixXd>(
        npy_array.data<double>(), n_rows, n_cols);
  }

}; // class NumpyReaderWriter


} // namespace KAF


