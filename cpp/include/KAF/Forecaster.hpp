#pragma once

#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm> // min_element

#include <Eigen/Core>

#include <KAF/Containers.hpp>
#include <KAF/ODE.hpp>
#include <KAF/LinearAlgebra.hpp>
#include <KAF/DiffusionOperator.hpp>


namespace KAF {

class Forecaster {
  /*
   * Forecast a measurement using the KAF diffusion operator
   */
private:
  const DiffusionOperator& diffusion_operator;
  std::unordered_map<unsigned, unsigned> truncations; // forecast_step -> n_eigenfuncs

  bool is_1st_way_cheaper(
      const size_t n_ic_points,
      const size_t n_forecast_steps)
  {
    const auto n_points = diffusion_operator.get_point_count();
    const auto n_eig    = diffusion_operator.get_eigen_count();

    const auto complexity_1st = n_points * (n_eig * n_ic_points + n_forecast_steps);
    const auto complexity_2nd = n_eig * n_forecast_steps * (n_points + n_ic_points);

    if (complexity_1st < complexity_2nd) {
      std::cerr << "~ [Forecaster] 1st way of computing is faster" << std::endl;

      const size_t memory_1st = 8 * n_eig * n_points * n_ic_points;
      constexpr size_t ten_gigabytes = 10737418240;
      if (memory_1st > ten_gigabytes) {
        std::cerr << "~ [Forecaster] requires > 10 GB, fall back to 2nd";
        std::cerr << std::endl;
      } else {
        return true;
      }
    } else {
      std::cerr << "~ [Forecaster] 2nd way of computing is faster" << std::endl;
    }

    return false;
  }

  template <typename BunchOrMatrix>
    DenseMatrix forecast_using_1st_way(
        const unsigned n_forecast_steps,
        const DenseMatrix& nystrom_extension,
        BunchOrMatrix&& measurement)
  {
    const auto n_points    = diffusion_operator.get_point_count();
    const auto n_ic_points = nystrom_extension.rows();
    const DenseMatrix& eigenvectors = diffusion_operator.get_eigenvectors();

    DenseMatrix prediction_series(n_ic_points, n_forecast_steps);

    std::vector<DenseMatrix> coefficients_tensor;
    for (unsigned m = 0; m < n_ic_points; ++m) {
      coefficients_tensor.push_back(
          nystrom_extension.row(m).asDiagonal() * eigenvectors.transpose());
    }

    for (unsigned k = 0; k < n_forecast_steps; ++k) {
      const unsigned ell = truncations.at(k);

      for (unsigned m = 0; m < n_ic_points; ++m) {
        const Eigen::VectorXd coefficients =
          coefficients_tensor[m].topRows(ell).colwise().sum();

        prediction_series(m,k) = vector_dot_measurement(
            coefficients, measurement, k);
      }
    }
    prediction_series /= n_points;

    return prediction_series;
  }

  template <typename BunchOrMatrix>
    DenseMatrix forecast_using_2nd_way(
        const unsigned n_forecast_steps,
        const DenseMatrix& nystrom_extension,
        BunchOrMatrix&& measurement)
  {
    const auto n_points    = diffusion_operator.get_point_count();
    const auto n_ic_points = nystrom_extension.rows();
    const DenseMatrix& eigenvectors = diffusion_operator.get_eigenvectors();

    DenseMatrix prediction_series(n_ic_points, n_forecast_steps);

    for (unsigned k = 0; k < n_forecast_steps; ++k) {
      const unsigned ell = truncations.at(k);

      const Eigen::VectorXd eigenvectors_times_measurement =
        matrix_times_measurement(
            eigenvectors.transpose().topRows(ell), measurement, k);

      prediction_series.col(k) =
        nystrom_extension.leftCols(ell) * eigenvectors_times_measurement;
    }
    prediction_series /= n_points;

    return prediction_series;
  }

  double vector_dot_measurement(
      const Eigen::VectorXd& coefficients,
      const TimeSeriesBunch& measurement,
      const unsigned forecast_step)
  {
    return measurement.dot_vector(coefficients, forecast_step);
  }

  double vector_dot_measurement(
      const Eigen::VectorXd& coefficients,
      const DenseMatrix& measurement,
      const unsigned forecast_step)
  {
    return measurement.col(forecast_step).dot(coefficients);
  }

  Eigen::VectorXd matrix_times_measurement(
      const DenseMatrix& matrix,
      const TimeSeriesBunch& measurement,
      const unsigned forecast_step)
  {
    return measurement.left_multiply_by_matrix(matrix, forecast_step);
  }

  Eigen::VectorXd matrix_times_measurement(
      const DenseMatrix& matrix,
      const DenseMatrix& measurement,
      const unsigned forecast_step)
  {
    return matrix * measurement.col(forecast_step);
  }

public:
  Forecaster(const DiffusionOperator& diffusion_operator) :
    diffusion_operator(diffusion_operator) {}

  template <typename BunchOrMatrix>
    DenseMatrix forecast(
        const unsigned n_forecast_steps,
        const DenseMatrix& nystrom_extension,
        BunchOrMatrix&& measurement)
  {
    const auto n_eig       = diffusion_operator.get_eigen_count();
    const auto n_ic_points = nystrom_extension.rows();

    if (nystrom_extension.cols() != n_eig) {
      throw std::logic_error(
          "[Forecaster::forecast] "
          "columns in Nystrom extension != number of eigenpairs: "
          + std::to_string(nystrom_extension.cols()));
    }
    /*
    if (measurement.get_timesteps() < n_points + n_forecast_steps) {
      throw std::logic_error(
          "[Forecaster::forecast] "
          "number of points in the measurement < needed for the forecast");
    }
    if (measurement.get_vector_dimension() != 1) {
      throw std::logic_error(
          "[Forecaster::forecast] can only forecast 1d measurement");
    }
    */

    const bool do_1st_way = is_1st_way_cheaper(n_ic_points, n_forecast_steps);
    if (do_1st_way) {
      return forecast_using_1st_way(
          n_forecast_steps,
          nystrom_extension,
          measurement);
    } else {
      return forecast_using_2nd_way(
          n_forecast_steps,
          nystrom_extension,
          measurement);
    }

  }

  DenseMatrix forecast_variance(
      const unsigned n_forecast_steps,
      const DenseMatrix& nystrom_extension_ic,
      const TimeSeriesBunch& measurement)
  {
    const auto n_points = diffusion_operator.get_point_count();
    auto&& eigenvectors = diffusion_operator.get_eigenvectors();

    DenseMatrix measurement_forecast_on_learn = forecast(
        n_forecast_steps,
        eigenvectors,
        measurement);

    DenseMatrix measurement_variance_on_learn(n_points, n_forecast_steps);
    for (unsigned k = 0; k < n_forecast_steps; ++k) {
      measurement_variance_on_learn.col(k) = measurement.minus_vector(
          measurement_forecast_on_learn.col(k), k);
    }
    measurement_variance_on_learn = measurement_variance_on_learn.cwiseAbs2();

    DenseMatrix measurement_variance_on_ic = forecast(
        n_forecast_steps,
        nystrom_extension_ic,
        measurement_variance_on_learn);

    return measurement_variance_on_ic;
  }

  void compute_and_store_truncations(
      const unsigned n_forecast_steps,
      const DenseMatrix& nystrom_extension_tune,
      const TimeSeriesBunch& measurement_tune,
      const TimeSeriesBunch& measurement_learn)
  {
    const auto n_points = diffusion_operator.get_point_count();
    const auto n_eig    = diffusion_operator.get_eigen_count();

    /*
    if (partition.get_extra_count() < n_forecast_steps) {
      throw std::logic_error(
          "[Forecaster::compute_and_store_truncations] "
          "number of extra points < needed for error eval");
    }
    */
    if (measurement_tune.get_vector_dimension() > 1) {
      throw std::logic_error(
          "[Forecaster::compute_and_store_truncations] "
          "can only tune using 1d measurements");
    }
    if (measurement_learn.get_vector_dimension() > 1) {
      throw std::logic_error(
          "[Forecaster::compute_and_store_truncations] "
          "can only tune 1d measurements");
    }

    /*
    const Eigen::VectorXd& v_measurement_learn = Eigen::VectorXd::Map(
        measurement_learn.get_data_pointer(),
        measurement_learn.get_timesteps());
    */

    const DenseMatrix& eigenvectors = diffusion_operator.get_eigenvectors();

    truncations.clear();
    truncations.reserve(n_forecast_steps);

    for (unsigned k = 0; k < n_forecast_steps; ++k) {
      std::vector<double> errors(n_eig);
      errors[0] = std::numeric_limits<double>::infinity();

      const Eigen::VectorXd eigenvectors_times_measurement_learn =
        measurement_learn.left_multiply_by_matrix(eigenvectors.transpose(), k);

      for (unsigned ell = 1; ell < n_eig; ++ell) {
        // XXX pre-multiply and then += columns instead
        const Eigen::VectorXd prediction =
          nystrom_extension_tune.leftCols(ell) *
          eigenvectors_times_measurement_learn.head(ell) /
          n_points;

        errors[ell] = measurement_tune.compute_dist_sq_from_vector(
            prediction, k);
      }
      const unsigned best_truncation =
        std::min_element(errors.begin(), errors.end()) - errors.begin();

      truncations.emplace(k, best_truncation);
    }

    print_truncations();
  }

  void print_truncations()
  {
    for (const auto& pair : truncations) {
      std::cerr << pair.first << " -> " << pair.second << std::endl;
    }
  }

  void set_truncations(const unsigned n_forecast_steps, const unsigned truncation)
  {
    truncations.clear();
    truncations.reserve(n_forecast_steps);

    for (unsigned k = 0; k < n_forecast_steps; ++k) {
      truncations.emplace(k, truncation);
    }

    print_truncations();
  }

}; // class Forecaster


} // namespace KAF


