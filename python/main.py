#!/opt/local/bin/python3 --

from timeit import default_timer as timer
import numpy as np
import scipy.integrate as scint
import scipy.linalg as sclin
from matplotlib import pyplot as plt

from numba import njit

def lorenz63_rhs(t, state, parameters):
  """
  Compute RHS of Lorenz '63 model

  Input:
    t (float): time
    state (np.array): (3,) vector of states
    parameters (dict): dictionary containing σ, ρ and β
  """
  rhs = np.empty(3)
  x, y, z = state
  sigma, rho, beta = parameters['sigma'], parameters['rho'], parameters['beta']

  rhs[0] = sigma * (y - x)
  rhs[1] = x * (rho - z) - y
  rhs[2] = x * y - beta * z

  return rhs

def double_well_rhs(t, state, parameters):
  """
  Compute RHS of double-well model with Lorenz '63 chaotic forcing

  Input:
    t (float): time
    state (np.array): (3,) vector of states
    parameters (dict): dictionary containing σ, ρ, β and ε
  """
  rhs = np.empty(4)
  x, y1, y2, y3 = state
  eps = parameters['eps']
  rhs[1:] = lorenz63_rhs(t, (y1, y2, y3), parameters)
  rhs[1:] /= eps*eps

  rhs[0] = x - x**3 + 4/(90*eps) * y2

  return rhs

@njit
def double_well_rhs_numba(t, state, parameters_array):
  """
  Compute RHS of double-well model with Lorenz'63 chaotic forcing, numba variant

  Input:
    t (float): time
    state (np.array): (3,) vector of states
    parameters_array (np.array): (4,) vector containing σ, ρ, β and ε
  """
  rhs = np.empty(4)
  x, y1, y2, y3 = state
  sigma, rho, beta, eps = parameters_array

  rhs[0] = x - x**3 + 4/(90*eps) * y2
  rhs[1] = sigma * (y2 - y1)
  rhs[2] = y1 * (rho - y3) - y2
  rhs[3] = y1 * y2 - beta * y3
  rhs[1:] /= eps*eps

  return rhs

def gauss_kernel(x, epsilon):
  """
  Compute kernel matrix on the data array using Gaussian kernel

  Input:
    x (np.array): (n_points,) or (dim, n_points) array
    epsilon (float): kernel parameter
  """
  from scipy.spatial.distance import squareform, pdist

  if x.ndim == 1:
    x_formatted = x[:,np.newaxis]
  else:
    x_formatted = x.T

  return np.exp( - squareform(pdist(x_formatted, 'sqeuclidean') / epsilon**2) )


################################################################################
# initialization ###############################################################
################################################################################
np.random.seed(42)

################################################################################
# constants ####################################################################
################################################################################
LORENZ63_DIM = 3
LORENZ63_PARAMETERS = {
    'sigma': 10,
    'rho'  : 28,
    'beta' : 8/3
}
DOUBLE_WELL_PARAMETERS = {
    'sigma': 10,
    'rho'  : 28,
    'beta' : 8/3,
    'eps'  : 0.1
}
DOUBLE_WELL_PARAMETERS_ARRAY = np.fromiter(
    DOUBLE_WELL_PARAMETERS.values(),
    dtype=np.float)

T_converged = 100
T_double_well_converged = 10
T = 6
dt_converged = 1e-2
dt = 1e-3

state_lorenz_ic = 10 * np.random.rand(LORENZ63_DIM) - 5

################################################################################
# main section #################################################################
################################################################################
lorenz63 = lambda t, state: lorenz63_rhs(t, state, LORENZ63_PARAMETERS)
double_well = lambda t, state: double_well_rhs(t, state, LORENZ63_PARAMETERS)

timer_lorenz = timer()
sol_converged = scint.solve_ivp(
    lorenz63,
    [0,T_converged],
    state_lorenz_ic,
    method='RK45',
    max_step=dt_converged)
state_lorenz_converged = sol_converged.y[:,-1]

sol_lorenz63 = scint.solve_ivp(
    lorenz63,
    [0,T],
    state_lorenz_converged,
    method='RK45',
    max_step=dt)
state_double_well_ic = np.hstack((state_lorenz_converged, np.random.rand()))
print("Lorenz 63 ended; elapsed:", timer() - timer_lorenz)

timer_double_well = timer()
sol_double_well_converged = scint.solve_ivp(
    double_well_rhs_numba,
    [0,T_double_well_converged],
    state_double_well_ic,
    args=(DOUBLE_WELL_PARAMETERS_ARRAY,),
    method='RK45',
    max_step=dt)
state_double_well_converged = sol_double_well_converged.y[:,-1]

sol_double_well = scint.solve_ivp(
    double_well_rhs_numba,
    [0,T],
    state_double_well_converged,
    args=(DOUBLE_WELL_PARAMETERS_ARRAY,),
    method='RK45',
    max_step=dt)
print("Double well ended; elapsed:", timer() - timer_double_well)

# kernel section ###############################################################
timer_kernel = timer()
G_lorenz63 = gauss_kernel(
    sol_lorenz63.y[0,:], 0.5)
G_double_well = gauss_kernel(
    sol_double_well.y[0,:], 0.5)
print("Kernel construction ended; elapsed:", timer() - timer_kernel)

G = G_double_well
n_points = G.shape[0]
print("Number of points:", n_points)

timer_eigen = timer()
D = G.sum(axis=0)
L = np.eye(n_points) - (G / D).T

G_eigvals, G_eigvecs = sclin.eig(L, check_finite=False)
G_eigvals = np.real(G_eigvals)

sorted_indices = np.argsort(G_eigvals)
G_eigvals = G_eigvals[sorted_indices]
G_eigvecs = np.real(G_eigvecs[:,sorted_indices])
print("Eigen problem ended; elapsed:", timer() - timer_eigen)


################################################################################
# plot section #################################################################
################################################################################
plt.plot(sol_lorenz63.t, sol_lorenz63.y[0,:], label='$x$')
plt.plot(sol_lorenz63.t, sol_lorenz63.y[1,:], label='$y$')
plt.plot(sol_lorenz63.t, sol_lorenz63.y[2,:], label='$z$')

plt.legend()
plt.show()

plt.plot(sol_lorenz63.y[0,:], sol_lorenz63.y[1,:])
plt.show()

plt.plot(sol_double_well.t, sol_double_well.y[0,:])
plt.show()

plt.plot(np.arange(G_eigvals.size), G_eigvals, '.')
plt.show()

plt.plot(np.arange(n_points), G_eigvecs[:,0], label='1st')
plt.plot(np.arange(n_points), G_eigvecs[:,1], label='2nd')
plt.plot(np.arange(n_points), G_eigvecs[:,2], label='3rd')
plt.plot(np.arange(n_points), G_eigvecs[:,3], label='4th')
plt.legend()
plt.show()



