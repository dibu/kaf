#!/bin/bash

#SBATCH --time=02:00:00                 # walltime
#SBATCH --ntasks=1                      # number of programs (tasks) to run in parallel
#SBATCH --nodes=1
#SBATCH --cpus-per-task=56
#SBATCH --mem=127G
#SBATCH -J "the_kaf"
#SBATCH --output=slurm_output/%A_%a.out
#SBATCH --error=slurm_output/%A_%a.err
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL


# create a directory named "slurm_output" and then submit with:
#       sbatch --mail-user=mail@domain.com run_the_kaf.sh


# preface ######################################################################
set -euo pipefail

# parameters & constants #######################################################
OUTPUT_DIR="output"
mkdir -p "${OUTPUT_DIR}"

stdout="${OUTPUT_DIR}/stdout"
stderr="${OUTPUT_DIR}/stderr"

# launch #######################################################################
module load openblas/0.3.6
module load arpack-ng/3.6.2

srun --unbuffered \
  bin/the_kaf >${stdout} 2>${stderr}


