#pragma once

#include <vector>
#include <deque>
#include <iostream> // cerr
#include <iterator> // make_move_iterator
#include <utility>  // move, forward, pair
#include <memory> // unique_ptr, make_unique, shared_ptr, make_shared
#include <stdexcept> // logic_error
#include <algorithm> // generate
#include <numeric> // accumulate

#include <Eigen/Core>
#include <Eigen/SparseCore>

#include <KAF/Partitioners.hpp> // Partition
#include <KAF/Utilities.hpp>


namespace KAF {

typedef Eigen::MatrixXd DenseMatrix;
typedef Eigen::SparseMatrix<double> SparseMatrix;


class Series;

class TimeSeries {

friend class Series; // for the move constructor Series(TimeSeries&&)

private:
  const size_t vector_dimension;
  size_t n_vectors = 0;

  std::vector<double> data_array; // size == n_vectors * vector_dimension
  std::vector<double> time_array; // size == n_vectors

public:
  TimeSeries() = delete;

  TimeSeries(const size_t vector_dimension, const size_t n_steps_to_reserve = 0)
    : vector_dimension(vector_dimension)
  {
    time_array.reserve(n_steps_to_reserve);
    data_array.reserve(n_steps_to_reserve * vector_dimension);
  }

  TimeSeries(const std::vector<double>& trajectory_1d, const double dt) :
    vector_dimension(1),
    n_vectors(trajectory_1d.size()),
    data_array(trajectory_1d)
  {
    time_array.resize(n_vectors);
    std::generate(
        time_array.begin(),
        time_array.end(),
        [&dt, i = 0] () mutable { return i++ * dt; });
  }

  template <typename T1, typename T2>
    TimeSeries(T1&& data_in, T2&& time_in, const size_t vector_dimension) :
      data_array(std::forward<T1>(data_in)),
      time_array(std::forward<T2>(time_in)),
      vector_dimension(vector_dimension)
  {
    n_vectors = time_array.size();
    if (n_vectors * vector_dimension != data_array.size()) {
      throw std::logic_error(
          "[TimeSeries::TimeSeries(const std::vector<double>&, ...)] "
          "time & data array sizes don't match (or vector dimension is wrong)");
    }
  }

  template <typename Tvec>
    void push_back(const double t, const Tvec& vector)
  {
    time_array.push_back(t);

    data_array.insert(
        std::end(data_array),
        std::begin(vector),
        std::end(vector));

    n_vectors++;
  }

  double operator[](const size_t index) const
  {
    /* In accordance with STL, no bounds checking */
    return data_array[index];
  }

  const double* get_vector_pointer(const size_t timestep) const
  {
    if (timestep >= n_vectors) {
      throw std::out_of_range(
          "[TimeSeries::get_vector_pointer] timestep >= n_vectors");
    }

    return data_array.data() + vector_dimension * timestep;
  }

  const double* get_data_pointer() const
  {
    return data_array.data();
  }

  TimeSeries coordinate_slice(const size_t vector_coordinate) const
  {
    /*
     * Returns [vector_coordinate] coordinate slice of the time series
     */
    if (vector_coordinate >= vector_dimension) {
      throw std::out_of_range(
          "[TimeSeries::coordinate_slice(const size_t)] "
          "coordinate >= dimension");
    }

    if (vector_dimension == 1) {
      return *this;
    }

    std::vector<double> sliced;
    sliced.reserve(n_vectors);
    auto data_array_index = vector_coordinate;
    for (size_t i = 0; i < n_vectors; ++i) {
      sliced.push_back(data_array[data_array_index]);
      data_array_index += vector_dimension;
    }

    return TimeSeries(std::move(sliced), time_array, 1);
  }

  TimeSeries coordinate_slice(
      const size_t start_coordinate,
      const size_t end_coordinate) const
  {
    /*
     * Returns [start, end) coordinate slice of the time series
     */
    if (end_coordinate > vector_dimension) {
      throw std::out_of_range(
          "[TimeSeries::coordinate_slice(const size_t, const size_t)] "
          "end_coordinate > vector_dimension");
    }
    if (start_coordinate >= end_coordinate) {
      throw std::logic_error(
          "[TimeSeries::coordinate_slice(const size_t, const size_t)] "
          "start_coordinate >= end_coordinate");
    }

    if (vector_dimension == 1) {
      return *this;
    }

    const size_t sliced_dimension = end_coordinate - start_coordinate;
    std::vector<double> sliced;
    sliced.reserve(sliced_dimension * n_vectors);
    auto data_array_start = data_array.begin() + start_coordinate;
    auto data_array_end   = data_array.begin() + end_coordinate;

    for (size_t i = 0; i < n_vectors; ++i) {
      sliced.insert(sliced.end(), data_array_start, data_array_end);
      data_array_start += vector_dimension;
      data_array_end   += vector_dimension;
    }

    return TimeSeries(std::move(sliced), time_array, sliced_dimension);
  }

  void throw_if_time_slice_invalid(
      const std::string& method,
      const size_t start_timestep,
      const size_t end_timestep) const
  {
    if (end_timestep > n_vectors) {
      throw std::out_of_range("[TimeSeries::" + method + "] end > n_vectors");
    }
    if (start_timestep > end_timestep) {
      throw std::out_of_range("[TimeSeries::" + method + "] start > end");
    }
  }

  TimeSeries time_slice(
      const size_t start_timestep,
      const size_t end_timestep) const
  {
    /*
     * The range is half-open: [start, end)
     */

    throw_if_time_slice_invalid("time_slice", start_timestep, end_timestep);
    if (start_timestep == end_timestep) {
      return TimeSeries(vector_dimension);
    }

    std::vector<double> data_sliced(
        data_array.begin() + vector_dimension * start_timestep,
        data_array.begin() + vector_dimension *   end_timestep);
    std::vector<double> time_sliced(
        time_array.begin() + start_timestep,
        time_array.begin() +   end_timestep);

    return TimeSeries(
        std::move(data_sliced),
        std::move(time_sliced),
        vector_dimension);
  }

  TimeSeries time_slice_from(const size_t start_timestep) const
  {
    /*
     * Short-hand for [start:]
     */
    return time_slice(start_timestep, n_vectors);
  }

  TimeSeries time_slice_until(const size_t end_timestep) const
  {
    /*
     * Short-hand for [:end]
     */
    return time_slice(0, end_timestep);
  }

  void erase_time_slice(const size_t start_timestep, const size_t end_timestep)
  {
    /*
     * The range is half-open: [start, end)
     */

    throw_if_time_slice_invalid("erase_time_slice", start_timestep, end_timestep);
    if (start_timestep == end_timestep) {
      return;
    }

    data_array.erase(
        data_array.begin() + vector_dimension * start_timestep,
        data_array.begin() + vector_dimension *   end_timestep);
    time_array.erase(
        time_array.begin() + start_timestep,
        time_array.begin() +   end_timestep);
    n_vectors -= end_timestep - start_timestep;

    return;
  }

  void erase_time_slice_from(const size_t start_timestep)
  {
    /*
     * Short-hand for [start:]
     */
    erase_time_slice(start_timestep, n_vectors);
    return;
  }

  void erase_time_slice_until(const size_t end_timestep)
  {
    /*
     * Short-hand for [:end]
     */
    erase_time_slice(0, end_timestep);
    return;
  }

  TimeSeries generate_delay_embedded_timeseries(const unsigned delay) const
  {
    if (delay == 0) {
      return *this;
    }

    const auto vector_dimension_delayed = (1 + delay) * vector_dimension;
    const auto timesteps_delayed = n_vectors - delay;

    if (delay >= n_vectors) {
      std::cerr << "[TimeSeries::generate_delay_embedded_timeseries] "
        << "delay >= n_vectors; returning empty TimeSeries" << std::endl;
      return TimeSeries(vector_dimension_delayed);
    }

    std::vector<double> data_delayed;
    data_delayed.reserve(vector_dimension_delayed * timesteps_delayed);

    for (size_t i = 0; i < timesteps_delayed; ++i) {
      data_delayed.insert(
          data_delayed.end(),
          data_array.begin() + vector_dimension * i,
          data_array.begin() + vector_dimension * i + vector_dimension_delayed);
    }

    std::vector<double> time_delayed(
        time_array.begin() + delay,
        time_array.end());

    return TimeSeries(
        std::move(data_delayed),
        std::move(time_delayed),
        vector_dimension_delayed);
  }

  template <typename Functor>
    TimeSeries generate_timeseries_by_applying(Functor&& func) const
  {
    /*
     * Functor is expected to be a callable with one (const double*) parameter
     * and returning (double).
     */
    std::vector<double> generated_data;
    generated_data.reserve(n_vectors);

    for (size_t k = 0; k < n_vectors; ++k) {
      generated_data.push_back(func(get_vector_pointer(k)));
    }

    return TimeSeries(std::move(generated_data), time_array, 1);
  }

  std::vector<double> get_time_array() const
  {
    return time_array;
  }

  size_t get_vector_dimension() const
  {
    return vector_dimension;
  }

  size_t size() const
  {
    return n_vectors;
  }

  size_t get_timesteps() const // XXX bad naming (either delete, or add _count)
  {
    return size();
  }

  TimeSeries& operator+=(const TimeSeries& rhs)
  {
    if (vector_dimension != rhs.vector_dimension) {
      throw std::logic_error(
          "[TimeSeries::operator+=] "
          "vector dimensions of lhs and rhs don't match");
    }

    time_array.insert(
        time_array.end(),
        rhs.time_array.begin(),
        rhs.time_array.end());

    data_array.insert(
        data_array.end(),
        rhs.data_array.begin(),
        rhs.data_array.end());

    n_vectors += rhs.n_vectors;

    return *this;
  }

  friend TimeSeries operator+(TimeSeries lhs, const TimeSeries& rhs)
  {
    lhs += rhs;
    return lhs;
  }

  friend std::ostream&
    operator<<(std::ostream& out, const TimeSeries& timeseries)
  {
    out << "total steps: " << timeseries.n_vectors << std::endl;
    for (size_t i = 0; i < timeseries.n_vectors; ++i) {
      out << timeseries.time_array[i] << ": ";
      for (size_t j = 0; j < timeseries.vector_dimension; ++j) {
        out << timeseries.data_array[i * timeseries.vector_dimension + j] << " ";
      }
      out << std::endl;
    }

    return out;
  }

}; // class TimeSeries


class Series {
private:
  typedef std::vector<double> t_vector;

  const size_t vector_dimension;
  size_t n_vectors = 0;

  std::shared_ptr<t_vector> p_data_array; // size == vector_dimension * n_vectors

public:
  Series() = delete;

  Series(const size_t vector_dimension, const size_t n_vectors_to_reserve = 0) :
    p_data_array(std::make_shared<t_vector>()),
    vector_dimension(vector_dimension)
  {
    p_data_array->reserve(vector_dimension * n_vectors_to_reserve);
  }

  Series(Series&& series) :
    p_data_array(std::move(series.p_data_array)),
    vector_dimension(series.vector_dimension),
    n_vectors(series.n_vectors) {}

  Series(const Series& series) :
    p_data_array(series.p_data_array),
    vector_dimension(series.vector_dimension),
    n_vectors(series.n_vectors) {}

  Series(TimeSeries&& timeseries) :
    p_data_array(
        std::make_shared<t_vector>( std::move(timeseries.data_array) )),
    vector_dimension(timeseries.vector_dimension),
    n_vectors(timeseries.n_vectors) {}

  Series(const TimeSeries& timeseries) :
    p_data_array(
        std::make_shared<t_vector>( timeseries.data_array )),
    vector_dimension(timeseries.vector_dimension),
    n_vectors(timeseries.n_vectors) {}

  template <typename T>
  Series(T&& data_in, const size_t vector_dimension) :
    p_data_array(
        std::make_shared<t_vector>( std::forward<T>(data_in) )),
    vector_dimension(vector_dimension),
    n_vectors(std::size(data_in) / vector_dimension)
  {
    if (vector_dimension * n_vectors != p_data_array->size()) {
      throw std::logic_error(
          "[Series::Series(T&&, const size_t)] "
          "array size is not a multiple of vector_dimension");
    }
  }

  double operator[](const size_t index) const
  {
    /* In accordance with STL, no bounds checking */
    return (*p_data_array)[index];
  }

  const double* get_vector_pointer(const size_t index) const
  {
    if (index >= n_vectors) {
      throw std::out_of_range(
          "[Series::get_vector_pointer] index >= n_vectors");
    }

    return p_data_array->data() + vector_dimension * index;
  }

  size_t get_vector_dimension() const
  {
    return vector_dimension;
  }

  size_t size() const
  {
    return n_vectors;
  }

  Series sequence_slice(const size_t start_index, const size_t end_index) const
  {
    /*
     * The range is half-open: [start, end)
     */
    if (end_index > n_vectors) {
      throw std::out_of_range("[Series::sequence_slice] end > n_vectors");
    }
    if (start_index > end_index) {
      throw std::out_of_range("[Series::sequence_slice] start > end");
    }
    if (start_index == end_index) {
      return Series(vector_dimension);
    }

    std::vector<double> data_sliced(
        p_data_array->begin() + vector_dimension * start_index,
        p_data_array->begin() + vector_dimension *   end_index);

    return Series(std::move(data_sliced), vector_dimension);
  }

  Series sequence_slice_from(const size_t start_index) const
  {
    /*
     * Short-hand for [start:]
     */
    return sequence_slice(start_index, n_vectors);
  }

  Series sequence_slice_until(const size_t end_index) const
  {
    /*
     * Short-hand for [:end]
     */
    return sequence_slice(0, end_index);
  }

  Series& operator+=(const Series& rhs)
  {
    if (vector_dimension != rhs.vector_dimension) {
      throw std::logic_error("[Series::operator+=(const Series&)] "
          "vector dimensions of lhs and rhs don't match");
    }

    p_data_array->insert(
        p_data_array->end(),
        rhs.p_data_array->begin(),
        rhs.p_data_array->end());

    n_vectors += rhs.n_vectors;

    return *this;
  }

  Series& operator+=(const TimeSeries& rhs)
  {
    if (vector_dimension != rhs.vector_dimension) {
      throw std::logic_error("[Series::operator+=(const TimeSeries&)] "
          "vector dimensions of lhs and rhs don't match");
    }

    p_data_array->insert(
        p_data_array->end(),
        rhs.data_array.begin(),
        rhs.data_array.end());

    n_vectors += rhs.n_vectors;

    return *this;
  }

  Series& operator+=(TimeSeries&& rhs)
  {
    if (vector_dimension != rhs.vector_dimension) {
      throw std::logic_error("[Series::operator+=(TimeSeries&&)] "
          "vector dimensions of lhs and rhs don't match");
    }

    p_data_array->insert(
        p_data_array->end(),
        std::make_move_iterator(rhs.data_array.begin()),
        std::make_move_iterator(rhs.data_array.end()));

    rhs.data_array.clear();
    rhs.time_array.clear();

    n_vectors += rhs.n_vectors;

    return *this;
  }

  friend Series operator+(Series lhs, const Series& rhs)
  {
    lhs += rhs;
    return lhs;
  }

  friend Series operator+(Series lhs, const TimeSeries& rhs)
  {
    lhs += rhs;
    return lhs;
  }

}; // class Series


class TimeSeriesBunch {
private:
  std::unique_ptr<Partition> p_partition;
  std::deque<TimeSeries> bunch;

  TimeSeriesBunch(const std::unique_ptr<Partition>& c_partition)
  {
    if (c_partition) {
      p_partition = std::make_unique<Partition>(*c_partition);
    }
  }

public:
  TimeSeriesBunch() {};

  bool empty() const
  {
    return bunch.empty();
  }

  void set_partition(Partition partition)
  {
    p_partition = std::make_unique<Partition>(std::move(partition));
  }

  void push_back(TimeSeries&& timeseries)
  {
    bunch.push_back(std::move(timeseries));
  }

  template <typename... Args>
    void emplace_back(Args&&... args)
  {
    bunch.emplace_back(std::forward<Args>(args)...);
  }

  const TimeSeries& operator[](const size_t index) const
  {
    return bunch[index];
  }

  size_t size() const
  {
    return bunch.size();
  }

  size_t get_vector_dimension() const
  {
    if (empty()) {
      std::cerr << "[TimeSeriesBunch::get_vector_dimension] "
        << "empty container, no vector dimension; returning 0" << std::endl;
      return 0;
    }

    const auto d_vector = bunch.front().get_vector_dimension();
    for (auto&& timeseries : bunch) {
      if (d_vector != timeseries.get_vector_dimension()) {
        std::cerr << "[TimeSeriesBunch::get_vector_dimension] "
          << "vector dimensions are different; returning 0" << std::endl;
        return 0;
      }
    }

    return d_vector;
  }

  void check_if_1dimensional(const std::string& prefix) const
  {
    if (get_vector_dimension() != 1) {
      throw std::logic_error(prefix + " ts-bunch is not 1-dimensional");
    }
  }

  size_t count_vectors() const
  {
    size_t n_total_vectors = 0;
    for (auto&& timeseries : bunch) {
      n_total_vectors += timeseries.get_timesteps();
    }
    return n_total_vectors;
  }

  size_t get_main_timesteps() const
  {
    const auto n_mains = get_main_counts();
    return std::accumulate(n_mains.begin(), n_mains.end(), size_t(0));
  }

  Series convert_to_series_and_empty()
  {
    /*
     * Returns concatenated Series object, leaving itself empty.
     */
    if (empty()) {
      return Series(get_vector_dimension());
    }

    Series concatenated_series( std::move(bunch.front()) );
    bunch.pop_front();

    const auto n_remaining_timeseries = bunch.size();
    for (size_t i = 0; i < n_remaining_timeseries; ++i) {
      concatenated_series += std::move(bunch.front());
      bunch.pop_front();
    }

    return concatenated_series;
  }

  TimeSeriesBunch coordinate_slice(const size_t vector_coordinate) const
  {
    TimeSeriesBunch sliced_bunch(p_partition);

    for (auto&& timeseries : bunch) {
      sliced_bunch.push_back(timeseries.coordinate_slice(vector_coordinate));
    }

    return sliced_bunch;
  }

  TimeSeriesBunch coordinate_slice(
      const size_t start_coordinate,
      const size_t end_coordinate) const
  {
    TimeSeriesBunch sliced_bunch(p_partition);

    for (auto&& timeseries : bunch) {
      sliced_bunch.push_back(
          timeseries.coordinate_slice(start_coordinate, end_coordinate));
    }

    return sliced_bunch;
  }

  TimeSeriesBunch time_slice_from(const size_t start_timestep) const
  {
    TimeSeriesBunch sliced_bunch(p_partition);

    for (auto&& timeseries : bunch) {
      sliced_bunch.push_back(
          timeseries.time_slice_from(start_timestep));
    }

    return sliced_bunch;
  }

  TimeSeriesBunch time_slice_tail(const size_t n_tail) const
  {
    TimeSeriesBunch tail_bunch(p_partition);

    for (auto&& timeseries : bunch) {
      const auto n_timesteps = timeseries.get_timesteps();
      tail_bunch.push_back(
          timeseries.time_slice_from(n_timesteps - n_tail));
    }

    return tail_bunch;
  }

  TimeSeriesBunch time_slice_not_tail(const size_t n_tail) const
  {
    TimeSeriesBunch not_tail_bunch(p_partition);

    for (auto&& timeseries : bunch) {
      const auto n_timesteps = timeseries.get_timesteps();
      not_tail_bunch.push_back(
          timeseries.time_slice_until(n_timesteps - n_tail));
    }

    return not_tail_bunch;
  }

  void erase_time_slice_tails(const size_t n_tail)
  {
    for (auto&& timeseries : bunch) {
      const auto n_timesteps = timeseries.get_timesteps();
      timeseries.erase_time_slice_from(n_timesteps - n_tail);
    }

    return;
  }

  TimeSeriesBunch generate_delay_embedded_bunch(const unsigned delay) const
  {
    if (empty()) {
      std::cerr << "[TimeSeriesBunch::generate_delay_embedded_bunch] "
        << "empty container; returning empty TimeSeriesBunch" << std::endl;
      return TimeSeriesBunch(p_partition);
    }

    TimeSeriesBunch bunch_delayed(p_partition);
    for (auto&& timeseries : bunch) {
      bunch_delayed.push_back(
          timeseries.generate_delay_embedded_timeseries(delay));
    }

    return bunch_delayed;
  }

  template <typename Functor>
    TimeSeriesBunch generate_bunch_by_applying(Functor&& func) const
  {
    if (empty()) {
      std::cerr << "[TimeSeriesBunch::generate_bunch_by_applying] "
        << "empty container; returning empty TimeSeriesBunch" << std::endl;
      return TimeSeriesBunch(p_partition);
    }

    TimeSeriesBunch generated_bunch(p_partition);
    for (auto&& timeseries : bunch) {
      generated_bunch.push_back(
          timeseries.generate_timeseries_by_applying(
            std::forward<Functor>(func)));
    }

    return generated_bunch;
  }

  std::vector<Eigen::Map<const Eigen::VectorXd>>
    get_timeseries_as_vectors() const
  {
    std::vector<Eigen::Map<const Eigen::VectorXd>> ts_as_vectors;

    for (auto&& timeseries : bunch) {
      const auto n_timesteps = timeseries.get_timesteps();
      const auto d_vector    = timeseries.get_vector_dimension();
      ts_as_vectors.emplace_back(
          timeseries.get_data_pointer(),
          n_timesteps * d_vector);
    }

    return ts_as_vectors;
  }

  std::vector<size_t> get_main_counts() const
  {
    if (!p_partition) {
      throw std::logic_error(
          "[TimeSeriesBunch::get_main_counts] "
          "partition is not set, cannot get main counts");
    }

    std::vector<size_t> n_mains;

    for (auto&& timeseries : bunch) {
      n_mains.push_back(p_partition->get_main_count(timeseries));
    }

    return n_mains;
  }

  double dot_vector(const Eigen::VectorXd& vector, const size_t offset) const
  {
    const auto ts_as_vectors = get_timeseries_as_vectors();
    const auto n_mains = get_main_counts();

    if (! is_vector_sum_equal_to_value(n_mains, vector.size()) ) {
      throw std::logic_error("[TimeSeriesBunch::dot_vector] "
          "vector size != sum of 'main' points");
    }
    check_if_1dimensional("[TimeSeriesBunch::dot_vector]");

    double result = 0.0;
    size_t global_index = 0;
    for (size_t i = 0; i < ts_as_vectors.size(); ++i) {
      result +=
        vector.segment(global_index, n_mains[i]).dot(
            ts_as_vectors[i].segment(offset, n_mains[i])
        );
      global_index += n_mains[i];
    }

    return result;
  }

  Eigen::VectorXd minus_vector(
      const Eigen::VectorXd& vector,
      const size_t offset) const
  {
    const auto ts_as_vectors = get_timeseries_as_vectors();
    const auto n_mains = get_main_counts();

    if (! is_vector_sum_equal_to_value(n_mains, vector.size()) ) {
      throw std::logic_error("[TimeSeriesBunch::minus_vector] "
          "vector size != sum of 'main' points");
    }
    check_if_1dimensional("[TimeSeriesBunch::minus_vector]");

    Eigen::VectorXd result(vector.size());
    size_t global_index = 0;
    for (size_t i = 0; i < ts_as_vectors.size(); ++i) {
      result.segment(global_index, n_mains[i]) =
        ts_as_vectors[i].segment(offset, n_mains[i]) -
        vector.segment(global_index, n_mains[i]);
      global_index += n_mains[i];
    }

    return result;
  }

  Eigen::VectorXd left_multiply_by_matrix(
      const DenseMatrix& matrix,
      const size_t offset) const
  {
    const auto ts_as_vectors = get_timeseries_as_vectors();
    const auto n_mains = get_main_counts();

    if (! is_vector_sum_equal_to_value(n_mains, matrix.cols()) ) {
      throw std::logic_error("[TimeSeriesBunch::left_multiply_by_matrix] "
          "# matrix columns != sum of 'main' points");
    }
    check_if_1dimensional("[TimeSeriesBunch::left_multiply_by_matrix]");

    Eigen::VectorXd result = Eigen::VectorXd::Zero(matrix.rows());
    size_t global_index = 0;
    for (size_t i = 0; i < ts_as_vectors.size(); ++i) {
      result +=
        matrix.middleCols(global_index, n_mains[i]) *
        ts_as_vectors[i].segment(offset, n_mains[i]);
      global_index += n_mains[i];
    }

    return result;
  }

  double compute_dist_sq_from_vector(
      const Eigen::VectorXd& vector,
      const size_t offset) const
  {
    const auto ts_as_vectors = get_timeseries_as_vectors();
    const auto n_mains = get_main_counts();

    if (! is_vector_sum_equal_to_value(n_mains, vector.size()) ) {
      throw std::logic_error("[TimeSeriesBunch::compute_dist_sq_from_vector] "
          "vector size != sum of 'main' points");
    }
    check_if_1dimensional("[TimeSeriesBunch::compute_dist_sq_from_vector]");

    double result = 0.0;
    size_t global_index = 0;
    for (size_t i = 0; i < ts_as_vectors.size(); ++i) {
      result += compute_distance_squared(
          vector.data() + global_index,
          ts_as_vectors[i].data() + offset,
          n_mains[i]);

      global_index += n_mains[i];
    }

    return result;
  }

}; // class TimeSeriesBunch


class LearnTunePartition {
  /*
   */

private:
  typedef std::pair<TimeSeriesBunch, TimeSeriesBunch> t_bunch_pair;

  size_t n_tune_main;
  size_t n_forecast;
  size_t n_delay;

  t_bunch_pair cut(TimeSeriesBunch&& bunch) const
  {
    const auto n_tune = n_tune_main + n_forecast;

    TimeSeriesBunch tune_with_delay = bunch.time_slice_tail(n_tune + n_delay);
    bunch.erase_time_slice_tails(n_tune);

    return { std::move(bunch), std::move(tune_with_delay) };
  }

public:
  LearnTunePartition() = delete;

  LearnTunePartition(
      const size_t n_tune_main,
      const size_t n_forecast,
      const size_t n_delay) :
    n_tune_main(n_tune_main), n_forecast(n_forecast), n_delay(n_delay) {}

  t_bunch_pair cut_data(TimeSeriesBunch&& bunch) const
  {
    auto&& [ learn, tune ] = cut( std::move(bunch) );

    // after the following erasures, they become '*_main' variants
    learn.erase_time_slice_tails(n_forecast);
    tune. erase_time_slice_tails(n_forecast);

    return { std::move(learn), std::move(tune) };
  }

  t_bunch_pair cut_measurement(TimeSeriesBunch&& bunch) const
  {
    return cut( std::move(bunch) );
  }

}; // class LearnTunePartition


class PairwiseDistances : public DenseMatrix {
  /*
   * Compute, store and output pairwise distances matrix.
   *
   * There are two variants:
   * (i) when only one timeseries/trajectory is supplied, the object is a square
   *  symmetric matrix with entries |x_i - x_j|, |x_i - x_j|^2 etc.
   * (ii) when two timeseries/trajectories are supplied, the object is a
   *  rectangular matrix with entries of the form |x_i - y_j| and so on, where
   *  x_i are elements of the first trajectory, y_j are elements of the second
   *  one.
   */
private:
  const bool symmetric;

  template <typename Functor>
  void populate_symmetric(Functor compute_ij_entry, const size_t dimension)
  {
    #pragma omp parallel for
    for (size_t j = 0; j < dimension; ++j) {
      for (size_t i = j + 1; i < dimension; ++i) {
        (*this)(i,j) = compute_ij_entry(i,j);
        (*this)(j,i) = (*this)(i,j); // XXX only for debugging; remove to save time
      }
    }

    for (size_t i = 0; i < dimension; ++i) {
      (*this)(i,i) = 0.0;
    }
  }

  template <typename Functor>
  void populate_symmetric(
      Functor compute_ij_entry,
      const size_t n_rows,
      const size_t n_cols)
  {
    if (n_rows != n_cols) {
      throw std::logic_error(
          "[PairwiseDistances::"
          "populate_symmetric(Functor, const size_t, const size_t)] "
          "number of rows and columns is not equal");
    }
    populate_symmetric(compute_ij_entry, n_rows);
  }

  template <typename Functor>
  void populate_nonsymmetric(
      Functor compute_ij_entry,
      const size_t n_rows,
      const size_t n_cols)
  {
    #pragma omp parallel for
    for (size_t j = 0; j < n_cols; ++j) {
      for (size_t i = 0; i < n_rows; ++i) {
        (*this)(i,j) = compute_ij_entry(i,j);
      }
    }
  }

public:
  PairwiseDistances() = delete;

  PairwiseDistances(DenseMatrix&& raw_distances, const bool symmetric) :
    DenseMatrix(std::move(raw_distances)),
    symmetric(symmetric)
  {
    if (symmetric && this->cols() != this->rows()) {
      throw std::logic_error(
          "[PairwiseDistances(DenseMatrix&&, const bool)] "
          "passed-argument matrix is not square, but symmetric == true");
    }
  }

  PairwiseDistances(const TimeSeries& timeseries) :
    DenseMatrix(timeseries.get_timesteps(), timeseries.get_timesteps()),
    symmetric(true)
  {
    const auto n_points = timeseries.get_timesteps();
    const auto d_vector = timeseries.get_vector_dimension();

    if (d_vector == 1) {
      auto compute_ij_entry = [&timeseries](const auto i, const auto j)
      {
        return compute_distance_squared(timeseries[i], timeseries[j]);
      };

      populate_symmetric(compute_ij_entry, n_points);
    } else {
      auto compute_ij_entry = [&timeseries, d_vector](const auto i, const auto j)
      {
        return compute_distance_squared(
            timeseries.get_vector_pointer(i),
            timeseries.get_vector_pointer(j),
            d_vector);
      };
      populate_symmetric(compute_ij_entry, n_points);
    }
  }

  PairwiseDistances(const Series& series) :
    DenseMatrix(series.size(), series.size()),
    symmetric(true)
  {
    const auto n_points = series.size();
    const auto d_vector = series.get_vector_dimension();

    if (d_vector == 1) {
      auto compute_ij_entry = [&series](const auto i, const auto j)
      {
        return compute_distance_squared(series[i], series[j]);
      };

      populate_symmetric(compute_ij_entry, n_points);
    } else {
      auto compute_ij_entry = [&series, d_vector](const auto i, const auto j)
      {
        return compute_distance_squared(
            series.get_vector_pointer(i),
            series.get_vector_pointer(j),
            d_vector);
      };
      populate_symmetric(compute_ij_entry, n_points);
    }
  }

  PairwiseDistances(
      const TimeSeries& vertical,
      const TimeSeries& horizontal,
      const bool symmetric = false) :
    DenseMatrix(vertical.get_timesteps(), horizontal.get_timesteps()),
    symmetric(symmetric)
  {
    if (vertical.get_vector_dimension() != horizontal.get_vector_dimension()) {
      throw std::logic_error(
          "[PairwiseDistances(const TimeSeries&, const TimeSeries&)] "
          "vector dimensions do not match");
    }

    const auto n_vertical   = vertical.get_timesteps();
    const auto n_horizontal = horizontal.get_timesteps();
    const auto d_vector     = vertical.get_vector_dimension();

    auto compute_ij_entry =
      [&vertical, &horizontal, d_vector](const auto i, const auto j)
    {
      return compute_distance_squared(
          vertical.get_vector_pointer(i),
          horizontal.get_vector_pointer(j),
          d_vector);
    };

    if (symmetric) {
      populate_symmetric(compute_ij_entry, n_vertical, n_horizontal);
    } else {
      populate_nonsymmetric(compute_ij_entry, n_vertical, n_horizontal);
    }
  }

  PairwiseDistances(
      const Series& vertical,
      const Series& horizontal,
      const bool symmetric = false) :
    DenseMatrix(vertical.size(), horizontal.size()),
    symmetric(symmetric)
  {
    if (vertical.get_vector_dimension() != horizontal.get_vector_dimension()) {
      throw std::logic_error(
          "[PairwiseDistances(const Series&, const Series&)] "
          "vector dimensions do not match");
    }

    const auto n_vertical   = vertical.size();
    const auto n_horizontal = horizontal.size();
    const auto d_vector     = vertical.get_vector_dimension();

    auto compute_ij_entry =
      [&vertical, &horizontal, d_vector](const auto i, const auto j)
    {
      return compute_distance_squared(
          vertical.get_vector_pointer(i),
          horizontal.get_vector_pointer(j),
          d_vector);
    };

    if (symmetric) {
      populate_symmetric(compute_ij_entry, n_vertical, n_horizontal);
    } else {
      populate_nonsymmetric(compute_ij_entry, n_vertical, n_horizontal);
    }
  }

  bool is_symmetric() const
  {
    return symmetric;
  }

  size_t get_point_count() const
  {
    if (this->cols() != this->rows()) {
      throw std::logic_error(
          "[PairwiseDistances::get_point_count()] "
          "this matrix is not square, choose horizontal or vertical direction");
    }
    return this->cols();
  }

  size_t get_vertical_point_count() const
  {
    return this->rows();
  }

  size_t get_horizontal_point_count() const
  {
    return this->cols();
  }

  void rescale_by(const double constant)
  {
    /*
     * In this version, 'constant' is a number applied to all elements:
     *      |x_i - y_j|^2 / constant
     */
    *this /= constant;
  }

  void rescale_by(const Eigen::ArrayXd& density)
  {
    /*
     * In this version, 'density' should be an array of size N (same as number
     * of points), and rescaling is done using:
     *      |x_i - x_j|^2 / (density[i] * density[j])
     */
    const auto n_points = get_point_count();

    if (density.size() != n_points) {
      throw std::length_error(
          "[PairwiseDistances::rescale_by(const ArrayXd&)] "
          "dimension of 'density' does not match");
    }

    Eigen::VectorXd density_inverse = density.inverse();
    DenseMatrix::operator=(
        density_inverse.asDiagonal()
        * (*this)
        * density_inverse.asDiagonal());
  }

  void rescale_by(
      const Eigen::ArrayXd& vertical_density,
      const Eigen::ArrayXd& horizontal_density)
  {
    /*
     * In this version, 'vertical_density' should be an array of size N (number
     * vertical points), 'horizontal_density' should be an array of size M
     * (number of horizontal points), and rescaling is done using:
     *      |x_i - y_j|^2 / (vertical_density[i] * horizontal_density[j])
     */
    const auto n_vertical_points   = get_vertical_point_count();
    const auto n_horizontal_points = get_horizontal_point_count();

    if (vertical_density.size() != n_vertical_points) {
      throw std::length_error(
          "[PairwiseDistances::rescale_by(const ArrayXd&, const ArrayXd&)] "
          "dimension of 'vertical_density' does not match");
    }

    if (horizontal_density.size() != n_horizontal_points) {
      throw std::length_error(
          "[PairwiseDistances::rescale_by(const ArrayXd&, const ArrayXd&)] "
          "dimension of 'horizontal_density' does not match");
    }

    Eigen::VectorXd vertical_density_inverse   = vertical_density.inverse();
    Eigen::VectorXd horizontal_density_inverse = horizontal_density.inverse();
    DenseMatrix::operator=(
        vertical_density_inverse.asDiagonal()
        * (*this)
        * horizontal_density_inverse.asDiagonal());
  }

  double compute_knn_sum(const size_t j, const size_t knn) const
  {
    /*
     * Returns the sum of the distances to the 'knn' nearest neighbors of the
     * jth horizontal point:
     *      Σ_{i \in I(j,knn)} |x_i - y_j|^2,
     * where I(j,knn) is the set of 'knn' vertical nearest neighbors of the
     * jth horizontal point.
     */
    if (this->IsRowMajor) {
      throw std::logic_error(
          "[PairwiseDistances::compute_knn_sum] "
          "matrix is not column-major, KNN sum cannot be computed");
      // in reality, it can be but needs a different (more expensive)
      // implementation
    }
    const auto n_vertical_points = get_vertical_point_count();

    Eigen::VectorXd sorted_knn_distances(knn);
    std::partial_sort_copy(
        this->col(j).data(),
        this->col(j).data() + n_vertical_points,
        sorted_knn_distances.data(),
        sorted_knn_distances.data() + knn);

    return sorted_knn_distances.sum();
  }

}; // class PairwiseDistances

} // namespace KAF


