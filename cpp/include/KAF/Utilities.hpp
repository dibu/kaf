#pragma once

#include <vector>
#include <numeric> // accumulate


namespace KAF {

double compute_distance_squared(
    const double* vector1, const double* vector2, const size_t dimension)
{
  double distance_squared = 0;

  for (size_t i = 0; i < dimension; ++i) {
    const auto coord_distance = vector1[i] - vector2[i];
    distance_squared += coord_distance * coord_distance;
  }

  return distance_squared;
}

double compute_distance_squared(const double scalar1, const double scalar2)
{
  return (scalar1 - scalar2) * (scalar1 - scalar2);
}

template <typename T, typename U>
  bool is_vector_sum_equal_to_value(const std::vector<T>& vec, const U value)
{
  const T sum = std::accumulate(vec.begin(), vec.end(), T(0));
  return (sum == value);
}

} // namespace KAF


