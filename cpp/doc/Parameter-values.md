# Known parameter values for various dynamical systems

This documents known parameter values that were found by running an optimization
procedure on datasets from various dynamical systems.
When running KAF, you may use these instead of running costly optimization every
time (whether automatic or 'manual', i.e. trial-and-error).

Here I provide two parameters for each system for the variable-bandwidth kernel,
the first optimization parameter (a.k.a. `EPSILON_GUESS`) and the second
optimization parameter (a.k.a. `DELTA_GUESS`); and one parameter for the RBF
kernel, bandwidth (a.k.a. `DELTA_GUESS`).

## Lorenz '63

1. Fixed ρ = 28 dataset
- VB:
  EPSILON_GUESS = 0.040048
  DELTA_GUESS   = 0.00150767
- RBF:
  DELTA_GUESS   = 1.0

2. Parametrically dependent on ρ dataset:
- VB:
  EPSILON_GUESS = 0.0868837
  DELTA_GUESS   = 0.00185557
- RBF: none

## Lorenz '96

- VB:
  EPSILON_GUESS = 0.39297
  DELTA_GUESS   = 0.035428
- RBF:
  DELTA_GUESS   = 8.855

## Double-well

- VB:
  EPSILON_GUESS = 0.0770644
  DELTA_GUESS   = 0.00897774
- RBF: none


