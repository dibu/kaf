#include <random>
#include <vector>
#include <array>

#include <KAF/Containers.hpp>
#include <KAF/ODE.hpp>

int main(int argc, const char** argv)
{
  typedef std::array<double, 3> state_type1;
  typedef std::vector<double>   state_type2;

  std::default_random_engine dre;
  std::uniform_real_distribution<double> unif(0, 1);

  KAF::TimeSeries timeseries(3, 1);

  const state_type1 ic = { 0.3, 0.7, 20.0 };
  timeseries.push_back(-0.1, ic);

  for (size_t i = 0; i < 128; ++i) {
    timeseries.push_back<state_type1>(
        i / 100.0,
        {unif(dre), unif(dre), unif(dre)});
  }

  timeseries.push_back<state_type1>(1.0, {34, 42, 19});
  timeseries.push_back<state_type1>(2.0, {1997, 88, -2.71828});
  timeseries.push_back<state_type2>(3.0, {99, 0.0001, -100});
  
  std::cout << timeseries << std::endl;

  return 0;
}


