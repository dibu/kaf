add_library(KAF INTERFACE)

target_include_directories(
  KAF
  INTERFACE .
            ${Boost_INCLUDE_DIRS})

target_link_libraries(
  KAF
  INTERFACE CONAN_PKG::cnpy
            CONAN_PKG::eigen)

