#pragma once

#include <cmath>
#include <iostream>
#include <variant>

#include <Eigen/Core>

#include <KAF/LinearAlgebra.hpp>
#include <KAF/ManifoldLearning.hpp>


namespace KAF {

class DiffusionOperator {
  /*
   * Construct the KAF diffusion operator; compute & store its eigenbasis
   */
public:
  typedef std::variant<SparseMatrix, DenseMatrix> GeneralMatrix;

private:
  const size_t n_points;

  SingularValueDecomposition K_tilde_svd;

  GeneralMatrix normalized_kernel_matrix;
  Eigen::VectorXd q_sqrt_inverse;

  void compute_normalized_kernel_matrix_and_q_sqrt_inverse(
      const SparseMatrix& kernel_matrix,
      const double sparse_dense_decider)
  {
    using Eigen::VectorXd, Eigen::pow;

    MatrixToolbox matrix_tool;

    VectorXd d_inverse = 1.0 / matrix_tool.compute_column_sums(kernel_matrix);
    this->q_sqrt_inverse = pow((kernel_matrix * d_inverse).array(), -0.5);

    const auto n_points = kernel_matrix.cols();

    if (kernel_matrix.nonZeros() > sparse_dense_decider * n_points * n_points) {
      //std::cout << "~~~ Dense" << std::endl;
      this->normalized_kernel_matrix = DenseMatrix(
          d_inverse.asDiagonal() * kernel_matrix * q_sqrt_inverse.asDiagonal());
    } else {
      //std::cout << "~~~ Sparse" << std::endl;
      this->normalized_kernel_matrix = SparseMatrix(
          d_inverse.asDiagonal() * kernel_matrix * q_sqrt_inverse.asDiagonal());
    }
  }

  GeneralMatrix compute_oos_normalized_kernel_matrix(
      const SparseMatrix& oos_kernel_matrix,
      const double sparse_dense_decider) const
  {
    MatrixToolbox matrix_tool;

    Eigen::VectorXd oos_d_inverse =
      1.0 / matrix_tool.compute_column_sums(oos_kernel_matrix);

    const auto n_rows = oos_kernel_matrix.rows();
    const auto n_cols = oos_kernel_matrix.cols();

    GeneralMatrix oos_normalized_kernel_matrix;

    if (oos_kernel_matrix.nonZeros() > sparse_dense_decider * n_rows * n_cols) {
      oos_normalized_kernel_matrix = DenseMatrix(
          q_sqrt_inverse.asDiagonal()
          * oos_kernel_matrix
          * oos_d_inverse.asDiagonal());
    } else {
      oos_normalized_kernel_matrix = SparseMatrix(
          q_sqrt_inverse.asDiagonal()
          * oos_kernel_matrix
          * oos_d_inverse.asDiagonal());
    }

    return oos_normalized_kernel_matrix;
  }

  DenseMatrix compute_point_evaluations(
      const GeneralMatrix& oos_normalized_kernel_matrix) const
  {
    auto matrix_transpose_times_right_singular_vectors =
      [this](auto&& matrix) -> DenseMatrix
      {
        return matrix.transpose() * this->K_tilde_svd.get_right_vectors();
      };

    DenseMatrix point_evaluations = std::visit(
        matrix_transpose_times_right_singular_vectors,
        oos_normalized_kernel_matrix);

    const Eigen::VectorXd&& eigenvalues_sqrt_inverse =
      get_eigenvalues_sqrt().cwiseInverse();
    point_evaluations *= eigenvalues_sqrt_inverse.asDiagonal();

    return point_evaluations;
  }

public:
  DiffusionOperator() = delete;

  DiffusionOperator(
      const SparseMatrix& kernel_matrix,
      const double sparse_dense_decider) :
    n_points(kernel_matrix.cols())
  {
    compute_normalized_kernel_matrix_and_q_sqrt_inverse(
        kernel_matrix,
        sparse_dense_decider);
  }

  void compute_and_store_eigenbasis(const unsigned n_eig)
  {
    MatrixToolbox matrix_tool;

    auto compute_svd = [&matrix_tool, n_eig](auto&& matrix)
    {
      return matrix_tool.compute_svd(matrix, n_eig);
    };

    this->K_tilde_svd = std::visit(compute_svd, normalized_kernel_matrix);
    this->K_tilde_svd.normalize_left_vectors( std::sqrt(n_points));
    this->K_tilde_svd.normalize_right_vectors(std::sqrt(n_points));
  }

  DenseMatrix compute_nystrom_extension(
      const SparseMatrix& oos_kernel_matrix,
      const double sparse_dense_decider) const
  {
    /* This takes in unnormalized kernel matrix of dimensions N x M, and outputs
     * eigenfunctions evaluated on M points in a form of a M x L matrix.
     *
     * N = learning points
     * M = new (IC/OOS) points
     * L = eigen-pairs
     */
    GeneralMatrix oos_normalized_kernel_matrix =
      compute_oos_normalized_kernel_matrix(
          oos_kernel_matrix,
          sparse_dense_decider);

    DenseMatrix point_evaluations = compute_point_evaluations(
        oos_normalized_kernel_matrix);

    return point_evaluations;
  }

  DenseMatrix compute_kernel_field(
      const SparseMatrix& oos_kernel_matrix,
      const double sparse_dense_decider) const
  {
    GeneralMatrix oos_normalized_kernel_matrix =
      compute_oos_normalized_kernel_matrix(
          oos_kernel_matrix,
          sparse_dense_decider);

    auto matrix_transpose_times_matrix_transpose =
      [this](auto&& matrix1, auto&& matrix2) -> DenseMatrix
      {
        return matrix1.transpose() * matrix2.transpose();
      };

    DenseMatrix kernel_field = std::visit(
        matrix_transpose_times_matrix_transpose,
        oos_normalized_kernel_matrix,
        this->normalized_kernel_matrix);

    return kernel_field;
  }

  const DenseMatrix& get_eigenvectors() const
  {
    return K_tilde_svd.get_left_vectors();
  }

  const Eigen::VectorXd& get_eigenvalues_sqrt() const
  {
    return K_tilde_svd.get_values();
  }

  size_t get_point_count() const
  {
    return n_points;
  }

  size_t get_eigen_count() const
  {
    return K_tilde_svd.get_count();
  }

}; // class DiffusionOperator


} // namespace KAF


