#pragma once

#include <iostream>
#include <vector>
#include <utility>   // pair
#include <tuple>     // tie
#include <stdexcept> // exception, logic_error

#include <nlopt.hpp>
#include <Eigen/Core>

/*
#define OPTIM_ENABLE_EIGEN_WRAPPERS
#include <optimlib/optim.hpp>
*/

#include <KAF/Containers.hpp>
#include <KAF/LinearAlgebra.hpp>
#include <KAF/Kernels.hpp>
#include <KAF/Auxiliary.hpp>


namespace KAF {

class KernelSumSlope {
  /*
   * Computes {m(eps), m'(eps)}, i.e. the slope of a kernel sum and its
   * derivative as a function of eps.
   * Maximizing the slope approximately equals to the manifold dimension, and
   * the value of eps that delivers said maximum is considered optimal [1].
   *
   * Used in minimizing/maximizing algorithms.
   *
   * [1] Coifman, Shkolnisky, Sigworth, Singer: Graph Laplacian tomography...
   */
  private:
    const PairwiseDistances& saved_pair_distances;

    Timer timer;
    double seconds_spent = 0.0;

    std::pair<double, double> compute_dimension_and_optional_gradient_no_timer(
        const double unconverted_eps,
        const bool compute_gradient) const
    {
      const double eps = unconverted_eps / stretch_eps;
      KernelEvaluator kernel_eval { GaussianKernel(eps) };

      // costly 1:
      auto kernel_matrix = kernel_eval.compute_dense_matrix(saved_pair_distances);
      /*
      auto kernel_matrix = kernel_eval.compute_sparse_matrix(saved_pair_distances, 0.3);
      kernel_matrix.makeCompressed();
      */

      // costly 2:
      const Eigen::ArrayXXd deps_kernel =
        kernel_matrix.array() * saved_pair_distances.array();
      /*
      SparseMatrix deps_kernel(kernel_matrix);
      for (int k = 0; k < deps_kernel.outerSize(); ++k) {
        for (SparseMatrix::InnerIterator it(deps_kernel, k); it; ++it) {
          it.valueRef() *= saved_pair_distances( it.row(), it.col() );
        }
      }
      */

      const double T_sum          = kernel_matrix.sum();
      const double PT_sum         = deps_kernel.sum();
      const double half_dimension = PT_sum / (eps * T_sum);

      std::cerr << "~ "
        << "T_sum: "     << std::setw(13) << T_sum << " | "
        << "eps: "       << std::setw(13) << eps   << " | "
        << "dimension: " << std::setw(13) << 2 * half_dimension;


      double gradient = 0.0;
      if (compute_gradient) {
        // costly 3:
        const double PPT_sum = (deps_kernel * saved_pair_distances.array()).sum();
        /*
        for (int k = 0; k < deps_kernel.outerSize(); ++k) {
          for (SparseMatrix::InnerIterator it(deps_kernel, k); it; ++it) {
            it.valueRef() *= saved_pair_distances( it.row(), it.col() );
          }
        }
        const double PPT_sum = deps_kernel.sum();
        */

        /*
        const double eps_squared = eps * eps;
        const double H   = T_sum;
        const double dH  = PT_sum / eps_squared;
        const double ddH =
          (-2.0 / eps) * dH + PPT_sum / (eps_squared * eps_squared);

        const double ds = (dH / H) + eps * (ddH * H - dH*dH) / (H*H);
        */

        const double PT_on_T = PT_sum / T_sum;
        const double ds =
          (PPT_sum / T_sum) - (eps * PT_on_T) - (PT_on_T * PT_on_T);

        gradient = ds / (stretch_eps * eps * eps * eps);
        std::cerr << " | gradient: " << std::setw(13) << gradient;
      }
      std::cerr << std::endl;

      return { 2 * half_dimension, gradient };
    }

  public:
    inline static double stretch_eps = 1.0;

    KernelSumSlope() = delete;
    KernelSumSlope(const PairwiseDistances& pair_distances) :
      saved_pair_distances(pair_distances)
    {
      seconds_spent = 0.0;
    }

    ~KernelSumSlope()
    {
      std::cerr << *this << std::endl;
    }

    std::pair<double, double> compute_dimension_and_gradient(
        const double unconverted_eps)
    {
      timer.restart();

      auto pair = compute_dimension_and_optional_gradient_no_timer(
          unconverted_eps, true);

      seconds_spent += timer.get_seconds();

      return pair;
    }

    double compute_dimension(const double unconverted_eps)
    {
      timer.restart();

      auto [dimension, _] = compute_dimension_and_optional_gradient_no_timer(
          unconverted_eps, false);

      seconds_spent += timer.get_seconds();

      return dimension;
    }

    double operator()(unsigned, const double* p_eps, double* p_gradient)
    {
      /*
       * This is in a format for NLopt library.
       * In:      p_eps
       * Out:     (return value), p_gradient
       *
       * Returns m(eps) and m'(eps), i.e. dimension and gradient.
       */
      const bool should_compute_gradient = !!p_gradient;

      if (should_compute_gradient) {
        auto [dimension, gradient] = compute_dimension_and_gradient(*p_eps);
        *p_gradient = gradient;
        return dimension;
      }

      return compute_dimension(*p_eps);
    }

    double operator()(const double eps)
    {
      /*
       * This is in a format for boost::math::tools::brent_find_minima.
       * This returns -m, i.e. minus dimension b/c it's fed into a
       * minima-finding algorithm.
       */
      return -compute_dimension(eps);
    }

    /*
    double operator()(
        const Eigen::VectorXd &v_eps,
        Eigen::VectorXd *v_gradient,
        void *data) const
    {
      //
      // This is in a format for OptimLib.
      // In:      v_eps, data (== nullptr)
      // Out:     (return value), v_gradient
      //
      // Returns -m(eps) and m'(eps), i.e. minus dimension and gradient.
      //
      timer.restart();

      const bool compute_gradient = !!v_gradient;

      auto [dimension, gradient] = compute_dimension_and_gradient(
          v_eps[0], compute_gradient);

      if (compute_gradient) {
        (*v_gradient)[0] = gradient;
      }

      seconds_spent += timer.get_seconds();
      return -dimension;
    }
    */

    friend std::ostream&
      operator<<(std::ostream& out, const KernelSumSlope& slope_computer)
    {
      out << "KernelSumSlope used: " << slope_computer.seconds_spent;
      return out;
    }

}; // class KernelSumSlope


class BandwidthDimensionOptimizer {
public:
  std::pair<double, double> operator()(
      const PairwiseDistances& pairwise_dces,
      const double initial_guess) const
  {
    /*
     * Estimate bandwidth parameter (a.k.a. epsilon) and dimension of the
     * manifold (a.k.a. m) by finding the max(s(epsilon)), where s = m/2.
     */

    const double stretch_eps = KernelSumSlope::stretch_eps;
    constexpr auto   ALGORITHM = nlopt::LN_COBYLA;
    constexpr size_t OPTIMIZATION_DIMENSION = 1;
    const     double LOWER_BOUND = 1e-7 * stretch_eps;
    const     double UPPER_BOUND = 1e2  * stretch_eps;
    constexpr double FUNCTION_VALUE_TOLERANCE = 1e-3;
    constexpr size_t MAX_ITERATIONS = 1000;

    nlopt::opt optimizer(ALGORITHM, OPTIMIZATION_DIMENSION);
    optimizer.set_lower_bounds(LOWER_BOUND);
    optimizer.set_upper_bounds(UPPER_BOUND);
    optimizer.set_ftol_rel(FUNCTION_VALUE_TOLERANCE);
    optimizer.set_maxeval(MAX_ITERATIONS);
    optimizer.set_vector_storage(10000);

    KernelSumSlope slope_computer(pairwise_dces);
    optimizer.set_max_objective(std::move(slope_computer));

    std::vector<double> epsilon_as_nlopt_arg = { stretch_eps * initial_guess };
    double dimension = 0.0;

    try {
      const auto result = optimizer.optimize(epsilon_as_nlopt_arg, dimension);
    } catch(const std::exception& e) {
      std::cerr << "NLopt failed: " << e.what() << std::endl;
    }

    return { epsilon_as_nlopt_arg[0] / stretch_eps, dimension };
  }

  /*
   * XXX OptimLib won't compile with Eigen 3.3.9
   *
  static std::pair<double, double> estimate_bandwidth_and_dimension_optim(
      const PairwiseDistances& pairwise_dces,
      const double initial_guess)
  {
    const double stretch_eps = KernelSumSlope::stretch_eps;

    Eigen::VectorXd epsilon_as_optim_arg(1);
    epsilon_as_optim_arg[0] = stretch_eps * initial_guess;

    KernelSumSlope slope_computer(pairwise_dces);
    const bool success = optim::de(epsilon_as_optim_arg, slope_computer, nullptr);
    if (!success) {
      std::cerr << "OptimLib failed!" << std::endl;
    }
    const double dimension = slope_computer.get_last_dimension();

    return { epsilon_as_optim_arg[0] / stretch_eps, dimension };
  }
  */

}; // class BandwidthDimensionOptimizer


class KernelDensityEstimator {
  /*
   * Estimate density 'sigma', bandwidth 'epsilon' and 'delta', and manifold
   * dimension 'm' from timeseries
   */
private:
  const unsigned knn;
  double m = 0;
  double epsilon = 0;
  Eigen::ArrayXd sigma;
  Eigen::ArrayXd rho; // == sigma^(-1/m)
  Eigen::ArrayXd adhoc_density;
  BandwidthDimensionOptimizer bandwidth_dimension;

  Eigen::ArrayXd compute_density(
      const PairwiseDistances& pairwise_dces,
      const Eigen::ArrayXd& local_adhoc_density) const
  {
    KernelEvaluator kernel_eval { GaussianKernel(this->epsilon) };
    auto kernel_matrix = kernel_eval.compute_dense_matrix(pairwise_dces);

    Eigen::ArrayXd base = M_PI * this->epsilon * local_adhoc_density.square();

    const auto n_vertical_points = pairwise_dces.get_vertical_point_count();
    Eigen::ArrayXd density = kernel_matrix.colwise().sum();
    density /= n_vertical_points * Eigen::pow(base, this->m / 2);

    return density;
  }

  Eigen::ArrayXd compute_rho() const
  {
    return compute_rho(this->sigma);
  }

  Eigen::ArrayXd compute_rho(const Eigen::ArrayXd& density) const
  {
    return Eigen::pow(density, - 1.0 / this->m);
  }

  static unsigned compute_knn(const double knn_ratio, const size_t n_points)
  {
    unsigned knn_candidate = knn_ratio * n_points;

    if (knn_candidate <= 1) {
      knn_candidate = 2; // the minimum possible number of neighbors
    } else if (knn_candidate >= n_points) {
      knn_candidate = n_points - 1; // the maximum possible number of neighbors
    }

    return knn_candidate;
  }

public:
  KernelDensityEstimator() = delete;

  KernelDensityEstimator(
      PairwiseDistances pairwise_dces,
      const double knn_ratio,
      const bool run_bandwidth_dimension_estimation,
      const double epsilon_initial_guess = 0.1) :
    knn( compute_knn(knn_ratio, pairwise_dces.get_point_count()) )
  {
    if (!pairwise_dces.is_symmetric()) {
      throw std::logic_error(
          "[KernelDensityEstimator(PairwiseDistances, const size_t)] "
          "pairwise_dces are expected to be symmetric");
    }

    std::cout << "Using " << knn << " nearest neighbors" << std::endl;
    adhoc_density = compute_adhoc_density(pairwise_dces, knn); // r_i

    pairwise_dces.rescale_by(adhoc_density); // |x_i - x_j| / (r_i * r_j)

    if (run_bandwidth_dimension_estimation)
    {
      Timer timer;
      std::tie(this->epsilon, this->m) = bandwidth_dimension(
          pairwise_dces, epsilon_initial_guess);
      std::cout << "Estimated epsilon and m: " << timer << std::endl;
    } else {
      this->epsilon = epsilon_initial_guess;
      KernelSumSlope slope_computer(pairwise_dces);
      this->m = slope_computer.compute_dimension(this->epsilon);
      std::cout << "Estimation of epsilon and m is turned off" << std::endl;
    }

    this->sigma = compute_density(pairwise_dces, adhoc_density);
    this->rho   = compute_rho();
  }

  double estimate_bandwidth(
      const PairwiseDistances& pairwise_dces,
      const double delta_initial_guess = 0.01) const
  {
    auto delta_and_dim = bandwidth_dimension(pairwise_dces, delta_initial_guess);
    return delta_and_dim.first;
  }

  static Eigen::ArrayXd compute_adhoc_density(
      const PairwiseDistances& pairwise_dces, const size_t knn)
  {
    /*
     * Returns the _horizontal_ adhoc density, i.e. if 'pairwise_dces' are:
     *      |x_i - y_j|^2,
     * then this returns a vector of t_j, i.e. densities of y_j.
     */
    const auto n_horizontal_points = pairwise_dces.get_horizontal_point_count();
    Eigen::ArrayXd horiz_adhoc_density(n_horizontal_points);

    #pragma omp parallel for
    for (size_t j = 0; j < n_horizontal_points; ++j) {
      horiz_adhoc_density[j] = pairwise_dces.compute_knn_sum(j, knn);
    }

    if (pairwise_dces.is_symmetric()) {
      horiz_adhoc_density /= (knn - 1);
    } else {
      horiz_adhoc_density /= knn;
    }
    horiz_adhoc_density = horiz_adhoc_density.sqrt();

    return horiz_adhoc_density;
  }

  Eigen::ArrayXd compute_oos_rho(PairwiseDistances oos_distances) const
  {
    const Eigen::ArrayXd oos_adhoc_density =
      compute_adhoc_density(oos_distances, knn); // t_j
    oos_distances.rescale_by(adhoc_density, oos_adhoc_density); // |x_i - y_j| / (r_i * t_j)

    const Eigen::ArrayXd oos_density =
      compute_density(oos_distances, oos_adhoc_density);

    return compute_rho(oos_density);
  }

  const Eigen::ArrayXd& get_rho() const
  {
    return this->rho;
  }

  const Eigen::ArrayXd& get_adhoc_density() const
  {
    return adhoc_density;
  }

  friend std::ostream& operator<<(
      std::ostream& out, const KernelDensityEstimator& kde)
  {
    out << "epsilon = " << kde.epsilon << "; ";
    out << "m = "       << kde.m;
    return out;
  }

}; // class KernelDensityEstimator


} // namespace KAF


