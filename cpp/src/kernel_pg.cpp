#include <iostream>
#include <string>
#include <vector>
#include <array>

#include <Eigen/Core>

#include <KAF/ODE.hpp>
#include <KAF/LinearAlgebra.hpp>
#include <KAF/Kernels.hpp>


int main(int argc, const char** argv)
{
  typedef std::array<double, 3> state_type;

  /****************************************************************************/
  std::vector<std::string> parameter_names = { "sigma", "rho", "beta" };
  std::vector<double> parameter_values = { 10, 28., 8./3. };

  KAF::ODEParameters lorenz63_parameters(
      parameter_names.begin(),
      parameter_values.begin(),
      3);

  std::cout << lorenz63_parameters << std::endl;

  typedef std::array<double, 3> state_type;
  KAF::Lorenz63<state_type> lorenz63;
  state_type lorenz63_ic = { 0.3, 0.7, 20.0 };

  KAF::ODEIntegratorParameters integrator_parameters(0.0, 40.0, 1e-2);
  KAF::ODEIntegrator integrator(integrator_parameters);

  KAF::TimeSeries lorenz63_result =
    integrator.run(lorenz63_ic, lorenz63, lorenz63_parameters);

  /****************************************************************************/
  KAF::PairwiseDistances pairwise_dces(lorenz63_result);
  auto n_points = pairwise_dces.get_point_count();
  std::cout << "n_points: " << n_points << std::endl;

  /****************************************************************************/
  KAF::TimeSeries timeseries(3);
  timeseries.push_back<state_type>(0.1, {34, 42, 19});
  timeseries.push_back<state_type>(0.2, {8.3, 92.1, 13.4});
  timeseries.push_back<state_type>(0.3, {22.17, 9, 20.33});
  timeseries.push_back<state_type>(0.4, {0.12, 8.8, 16});

  KAF::GaussianKernel gaussian_kernel(0.01);
  KAF::KernelEvaluator kernel_eval(gaussian_kernel);

  /****************************************************************************/
  KAF::MatrixToolbox matrix_tool;
  constexpr size_t N_EIG = 7;
  auto kernel_matrix = kernel_eval.compute_dense_matrix(pairwise_dces);
  auto eigen_decomposition = matrix_tool.compute_ed_symmetric(kernel_matrix, N_EIG);

  std::cout << eigen_decomposition;

  /****************************************************************************/
  KAF::PairwiseDistances pairwise_dces_2(timeseries);
  auto kernel_matrix_2 = kernel_eval.compute_sparse_matrix(pairwise_dces_2, 0.5);

  return 0;
}


