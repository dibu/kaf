#pragma once

#include <iterator> // size
#include <stdexcept> // logic_error

#include <KAF/Containers.hpp>


namespace KAF {

class Partition {
  /*
   * Partition specifies how SeriesContainer (Series, TimeSeries etc.) is split
   * into two parts:
   * |          main          |     extra     |
   * |------------------------|---------------|
   *
   * Only the length of 'extra' is specified; the 'main' is computed from a
   * particular (time)series' length.
   *
   * This class also extracts corresponding parts from (Time)Series.
   */

private:
  size_t n_extra;

public:
  Partition() = delete;

  Partition(const size_t n_extra) : n_extra(n_extra) {}

  size_t get_extra_count() const
  {
    return n_extra;
  }

  /*
  size_t get_main_count(const Series& series) const
  {
    const auto n_vectors = series.size();

    if (n_vectors < n_extra) {
      throw std::logic_error(
          "[Partition::get_main_count(const Series&)] n_vectors < n_extra");
    }

    return n_vectors - n_extra;
  }
  */

  template <typename SeriesContainer>
  size_t get_main_count(const SeriesContainer& series) const
  {
    const auto n_vectors = std::size(series);

    if (n_vectors < n_extra) {
      throw std::logic_error(
          "[Partition::get_main_count(const SeriesContainer&)] n_vectors < n_extra"
      );
    }

    return n_vectors - n_extra;
  }

  /*
  Series extract_extra(const Series& series) const
  {
    const auto n_main = get_main_count(series);
    return series.sequence_slice_from(n_main);
  }

  Series extract_main(const Series& series) const
  {
    const auto n_main = get_main_count(series);
    return series.sequence_slice_until(n_main);
  }

  TimeSeries extract_extra(const TimeSeries& timeseries) const
  {
    const auto n_main = get_main_count(timeseries);
    return timeseries.time_slice_from(n_main);
  }

  TimeSeries extract_main(const TimeSeries& timeseries) const
  {
    const auto n_main = get_main_count(timeseries);
    return timeseries.time_slice_until(n_main);
  }
  */

}; // class Partition

} // namespace KAF


