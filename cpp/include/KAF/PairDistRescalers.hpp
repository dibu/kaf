#pragma once

#include <utility> // move

#include <KAF/Containers.hpp>


namespace KAF {

class PairDistRescalerForGaussian {
  /*
   * A class that rescales a PairwiseDistances object for the Gaussian kernel.
   */
private:
  const double scale_parameter;

public:
  PairDistRescalerForGaussian() = delete;

  PairDistRescalerForGaussian(const double scale_parameter) :
    scale_parameter(scale_parameter) {}

  void apply(PairwiseDistances& pair_distances) const
  {
    pair_distances.rescale_by(scale_parameter);
  }

}; // class PairDistRescalerForGaussian


template <typename KDEType>
class PairDistRescalerForVB {
  /*
   * A class that rescales a PairwiseDistances object for the variable-bandwidth
   * kernel.
   *
   * Pairwise distances are assumed of the following form:
   *    |x_i - y_j|^2,
   * where {x_i} were used for density estimation (i.e. learning points).
   */
private:
  const KDEType kde;
  const double scale_parameter;

public:
  PairDistRescalerForVB() = delete;

  PairDistRescalerForVB(KDEType&& kde, const double scale_parameter) :
    kde(std::move(kde)),
    scale_parameter(scale_parameter) {}

  void apply(PairwiseDistances& pair_distances) const
  {
    auto&& oos_rho = kde.compute_oos_rho(pair_distances);
    pair_distances.rescale_by(kde.get_rho(), oos_rho);
    pair_distances.rescale_by(scale_parameter);
  }

}; // class PairDistRescalerForVB

} // namespace KAF


