#!/opt/local/bin/python3 --

from timeit import default_timer as timer
import numpy as np
import scipy.integrate as scint
from matplotlib import pyplot as plt

from numba import njit

def lorenz63_rhs(t, state, parameters_array):
  """
  Compute RHS of Lorenz '63 model

  Input:
    t (float): time
    state (np.array): (3,) vector of states
    parameters_array (np.array): (3,) vector containing σ, ρ and β
  """
  rhs = np.empty(3)
  x, y, z = state
  sigma, rho, beta = parameters_array

  rhs[0] = sigma * (y - x)
  rhs[1] = x * (rho - z) - y
  rhs[2] = x * y - beta * z

  return rhs

def double_well_rhs(t, state, parameters_array):
  """
  Compute RHS of double-well model with Lorenz '63 chaotic forcing

  Input:
    t (float): time
    state (np.array): (3,) vector of states
    parameters_array (np.array): (4,) vector containing σ, ρ, β and ε
  """
  rhs = np.empty(4)
  x, y2 = state[0], state[2]
  eps = parameters_array[3]

  rhs[1:] = lorenz63_rhs(t, state[1:], parameters_array[:3])
  rhs[1:] /= eps*eps

  rhs[0] = x - x**3 + 4/(90*eps) * y2

  return rhs

@njit
def double_well_rhs_numba(t, state, parameters_array):
  """
  Compute RHS of double-well model with Lorenz'63 chaotic forcing, numba variant

  Input:
    t (float): time
    state (np.array): (4,) vector of states
    parameters_array (np.array): (4,) vector containing σ, ρ, β and ε
  """
  rhs = np.empty(4)
  x, y1, y2, y3 = state
  sigma, rho, beta, eps = parameters_array

  rhs[0] = x - x**3 + 4/(90*eps) * y2
  rhs[1] = sigma * (y2 - y1)
  rhs[2] = y1 * (rho - y3) - y2
  rhs[3] = y1 * y2 - beta * y3
  rhs[1:] /= eps*eps

  return rhs

def double_well_jacobian(t, state, parameters_array):
  """
  Compute Jacobian of double-well model with Lorenz'63 chaotic forcing

  Input:
    t (float): time
    state (np.array): (4,) vector of states
    parameters_array (np.array): (4,) vector containing σ, ρ, β and ε
  """
  x, y1, y2, y3 = state
  sigma, rho, beta, eps = parameters_array

  jacobian = np.zeros((4,4))

  jacobian[1,1] = -sigma
  jacobian[1,2] =  sigma

  jacobian[2,1] = rho - y3
  jacobian[2,2] = -1
  jacobian[2,3] = -y1

  jacobian[3,1] = y2
  jacobian[3,2] = y1
  jacobian[3,3] = -beta

  jacobian /= eps*eps

  jacobian[0,0] = 1 - 3*x*x
  jacobian[0,2] = 4/(90*eps)

  return jacobian

@njit
def double_well_jacobian_numba(t, state, parameters_array):
  """
  Compute Jacobian of double-well model with Lorenz'63 chaotic forcing, numba
  variant

  Input:
    t (float): time
    state (np.array): (4,) vector of states
    parameters_array (np.array): (4,) vector containing σ, ρ, β and ε
  """
  #x, y1, y2, y3 = state
  x  = state[0]
  y1 = state[1]
  y2 = state[2]
  y3 = state[3]

  #sigma, rho, beta, eps = parameters_array
  sigma = parameters_array[0]
  rho   = parameters_array[1]
  beta  = parameters_array[2]
  eps   = parameters_array[3]

  jacobian = np.zeros((4,4))

  jacobian[1,1] = -sigma
  jacobian[1,2] =  sigma

  jacobian[2,1] = rho - y3
  jacobian[2,2] = -1
  jacobian[2,3] = -y1

  jacobian[3,1] = y2
  jacobian[3,2] = y1
  jacobian[3,3] = -beta

  jacobian /= eps*eps

  jacobian[0,0] = 1 - 3*x*x
  jacobian[0,2] = 4/(90*eps)

  return jacobian


################################################################################
# initialization ###############################################################
################################################################################
np.random.seed(42)

################################################################################
# constants ####################################################################
################################################################################
RUN_LORENZ63   = False
RUN_DOUBLEWELL = True

if RUN_LORENZ63:
  LORENZ63_DIM = 3
  LORENZ63_PARAMETERS = {
      'sigma': 10,
      'rho'  : 28,
      'beta' : 8/3
  }
  state_lorenz_ic = 10 * np.random.rand(LORENZ63_DIM) - 5

if RUN_DOUBLEWELL:
  DOUBLEWELL_DIM = 4
  DOUBLE_WELL_PARAMETERS = {
      'sigma': 10,
      'rho'  : 28,
      'beta' : 8/3,
      #'eps'  : 0.03162277660168379
      'eps'  : 0.1
  }
  DOUBLE_WELL_PARAMETERS_ARRAY = np.fromiter(
      DOUBLE_WELL_PARAMETERS.values(),
      dtype=float)
  #state_double_well_ic = np.array([0.19, 0.3, 0.7, 20.0])
  state_double_well_ic = np.random.rand(DOUBLEWELL_DIM)

T_converged_lorenz63   = 100
T_converged_doublewell = 1
T = 1000
dt_converged = 1e-2
dt = 5e-2
evaluation_points = np.arange(int(T/dt) + 1) * dt # [0, dt, 2*dt,..., T]


################################################################################
# main section #################################################################
################################################################################
if RUN_LORENZ63:
  lorenz63 = lambda t, state: lorenz63_rhs(t, state, LORENZ63_PARAMETERS)
  timer_lorenz = timer()
  sol_converged = scint.solve_ivp(
      lorenz63,
      [0, T_converged_lorenz63],
      state_lorenz_ic,
      method='RK45',
      max_step=dt_converged)
  state_lorenz_converged = sol_converged.y[:,-1]

  sol_lorenz63 = scint.solve_ivp(
      lorenz63,
      [0, T],
      state_lorenz_converged,
      t_eval=evaluation_points,
      method='RK45',
      max_step=dt)
  print("Lorenz 63:", timer() - timer_lorenz)

if RUN_DOUBLEWELL:
  double_well = lambda t, state: double_well_rhs(t, state, LORENZ63_PARAMETERS)
  timer_double_well = timer()

  sol_double_well_converged = scint.solve_ivp(
      double_well_rhs_numba,
      [0, T_converged_doublewell],
      state_double_well_ic,
      args=(DOUBLE_WELL_PARAMETERS_ARRAY,),
      method='RK45',
      jac=double_well_jacobian_numba,
      max_step=dt)
  state_double_well_converged = sol_double_well_converged.y[:,-1]
  print("Double well convergence:", timer() - timer_double_well)

  sol_double_well = scint.solve_ivp(
      double_well_rhs_numba,
      [0, T],
      state_double_well_converged,
      rtol=1e-9,
      atol=1e-9,
      args=(DOUBLE_WELL_PARAMETERS_ARRAY,),
      t_eval=evaluation_points,
      method='lsoda',
      jac=double_well_jacobian_numba)
      #max_step=dt)
  print("Double well total:", timer() - timer_double_well)

################################################################################
# plot section #################################################################
################################################################################
if RUN_LORENZ63:
  plt.plot(sol_lorenz63.t, sol_lorenz63.y[0,:], label='$x$')
  plt.plot(sol_lorenz63.t, sol_lorenz63.y[1,:], label='$y$')
  plt.plot(sol_lorenz63.t, sol_lorenz63.y[2,:], label='$z$')

  plt.legend()
  plt.show()

  plt.plot(sol_lorenz63.y[0,:], sol_lorenz63.y[1,:])
  plt.show()

if RUN_DOUBLEWELL:
  plt.plot(sol_double_well.t, sol_double_well.y[0,:])
  plt.show()

################################################################################
# save section #################################################################
################################################################################
if RUN_LORENZ63:
  np.save('lorenz63_trajectory.npy', sol_lorenz63.y)
  np.save('lorenz63_times.npy',      sol_lorenz63.t)

if RUN_DOUBLEWELL:
  np.save('doublewell_trajectory.npy', sol_double_well.y[0,:])
  np.save('doublewell_times.npy',      sol_double_well.t)


