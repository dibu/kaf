#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <variant>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <KAF/ODE.hpp>
#include <KAF/LinearAlgebra.hpp>
#include <KAF/Kernels.hpp>
#include <KAF/ManifoldLearning.hpp>
#include <KAF/Auxiliary.hpp>


KAF::DenseMatrix generate_oos_points_lorenz63(const size_t n_oos_points)
{
  using Eigen::VectorXd;

  KAF::DenseMatrix oos_points = KAF::DenseMatrix::Random(3, n_oos_points);

  oos_points.row(0) *= 20; // [-20,20]
  oos_points.row(1) *= 20; // [-20,20]
  oos_points.row(2) *= 20;
  oos_points.row(2) += VectorXd::Constant(n_oos_points, 25); // [5, 45]

  return oos_points;
}

Eigen::VectorXd generate_oos_points_doublewell(const size_t n_oos_points)
{
  return Eigen::VectorXd::LinSpaced(n_oos_points, -1.5, 1.5);
}

KAF::SparseMatrix construct_fd_generator_stencil(const size_t n)
{
  /*
   * Constructs stencil of the finite-difference approximation of the generator
   * action, i.e. V_bar:
   *        V_bar_j     = <-1, 0, 1>    for j in [2, n-3],
   *        V_bar_0     = <0, 1/2>
   *        V_bar_1     = <-1/2, 0, 1>
   *        V_bar_{n-2} = <-1, 0, 1/2>
   *        V_bar_{n-1} = <-1/2, 0>
   */
  constexpr double HALF = 0.5;
  constexpr double ONE  = 1.0;
  std::vector<Eigen::Triplet<double> > triplets;
  triplets.reserve(2*n - 2);
  triplets.emplace_back(0, 1,  HALF);
  triplets.emplace_back(1, 0, -HALF);
  triplets.emplace_back(1, 2,   ONE);
  triplets.emplace_back(n-2, n-3,  -ONE);
  triplets.emplace_back(n-2, n-1,  HALF);
  triplets.emplace_back(n-1, n-2, -HALF);
  for (size_t i = 2; i < n-2; ++i) {
    triplets.emplace_back(i, i-1, -ONE);
    triplets.emplace_back(i, i+1,  ONE);
  }

  KAF::SparseMatrix stencil(n, n);
  stencil.setFromTriplets(triplets.begin(), triplets.end());

  return stencil;
}

int main(int argc, const char** argv)
{
  typedef std::array<double, 4> state_type;
  //typedef std::array<double, 3> state_type;
  using Eigen::ArrayXd, Eigen::VectorXd, Eigen::MatrixXd;

  /****************************************************************************/
  KAF::Timer timer;
  constexpr double DT = 1e-2;

  /*
  std::vector<std::string> parameter_names = { "sigma", "rho", "beta" };
  std::vector<double> parameter_values = { 10, 28., 8./3. };

  KAF::ODEParameters lorenz63_parameters(
      parameter_names.begin(),
      parameter_values.begin(),
      3);

  std::cout << lorenz63_parameters << std::endl;

  typedef std::array<double, 3> state_type;
  KAF::Lorenz63<state_type> lorenz63;
  state_type lorenz63_ic = { 0.3, 0.7, 20.0 };

  KAF::ODEIntegratorParameters integrator_parameters(0.0, 50.0, DT);
  KAF::ODEIntegrator integrator(integrator_parameters);

  KAF::TimeSeries lorenz63_result =
    integrator.run(lorenz63_ic, lorenz63, lorenz63_parameters);

  std::cout << "Lorenz'63 integration: " << timer << std::endl;
  std::cout << "Timesteps: " << lorenz63_result.get_timesteps() << std::endl;
  std::cout << std::endl;

  const auto& doublewell_x = lorenz63_result;
  */

  std::vector<std::string> parameter_names =
    { "sigma", "rho", "beta", "epsilon" };
  std::vector<double> parameter_values =
    {     10.,   28.,   8./3,      1e-1 };

  KAF::ODEParameters doublewell_parameters(
      parameter_names.begin(),
      parameter_values.begin(),
      4);

  std::cout << doublewell_parameters << std::endl;

  KAF::DoubleWell<state_type> doublewell;
  state_type doublewell_ic = { 0.1, 0.3, 0.7, 20.0 };

  KAF::ODEIntegratorParameters integrator_parameters(0.0, 30.0, DT);
  KAF::ODEIntegrator integrator(integrator_parameters);

  KAF::TimeSeries doublewell_result =
    integrator.run(doublewell_ic, doublewell, doublewell_parameters);

  std::cout << "Double-well integration: " << timer << std::endl;
  std::cout << "Timesteps: " << doublewell_result.get_timesteps() << std::endl;
  std::cout << std::endl;

  const auto& doublewell_x = doublewell_result.coordinate_slice(0);

  /****************************************************************************/
  timer.restart();
  constexpr size_t KNN = 8;

  KAF::PairwiseDistances pairwise_dces(doublewell_x);

  KAF::KernelDensityEstimator kde(pairwise_dces, KNN);
  std::cout << kde << std::endl;

  pairwise_dces.rescale_by(kde.get_rho());
  const auto delta = kde.estimate_bandwidth(pairwise_dces);
  std::cout << "delta: " << delta << std::endl;

  std::cout << "Kernel density estimation: " << timer << std::endl;
  std::cout << std::endl;

  /****************************************************************************/
  timer.restart();

  const auto n_points = pairwise_dces.get_point_count();
  KAF::MatrixToolbox matrix_tool;

  // step 1
  KAF::KernelEvaluator kernel_eval( (KAF::GaussianKernel(delta)) ); // MVP
  auto kernel_matrix = kernel_eval.compute_sparse_matrix(pairwise_dces, 0.5);
  //auto kernel_matrix = kernel_eval.compute_dense_matrix(pairwise_dces);
  kernel_matrix /= n_points;
  // step 2
  VectorXd d_inverse = 1.0 / matrix_tool.compute_column_sums(kernel_matrix);
  VectorXd q_sqrt_inverse = Eigen::pow((kernel_matrix * d_inverse).array(), -0.5);
  // step 3
  std::variant<KAF::SparseMatrix, KAF::DenseMatrix> normalized_kernel_matrix;
  if (kernel_matrix.nonZeros() > 0.1 * n_points * n_points) {
    std::cout << "~~~ Dense" << std::endl;
    normalized_kernel_matrix =
      KAF::DenseMatrix(
          d_inverse.asDiagonal() * kernel_matrix * q_sqrt_inverse.asDiagonal());
  } else {
    std::cout << "~~~ Sparse" << std::endl;
    normalized_kernel_matrix =
      KAF::SparseMatrix(
          d_inverse.asDiagonal() * kernel_matrix * q_sqrt_inverse.asDiagonal());
  }

  std::cout << "Steps 1-3: " << timer << std::endl;
  std::cout << std::endl;

  timer.restart();
  // step 4
  KAF::SingularValueDecomposition svd;
  constexpr size_t N_SING = 50;
  if (std::holds_alternative<KAF::DenseMatrix>(normalized_kernel_matrix)) {
    std::cout << "~~~ Dense" << std::endl;
    svd = std::move(
        matrix_tool.compute_svd(
          std::get<KAF::DenseMatrix>(normalized_kernel_matrix), N_SING));
  } else {
    std::cout << "~~~ Sparse" << std::endl;
    svd = std::move(
        matrix_tool.compute_svd(
          std::get<KAF::SparseMatrix>(normalized_kernel_matrix), N_SING));
  }

  std::cout << "Total SVD: " << timer << std::endl;
  std::cout << std::endl;

  /****************************************************************************/
  timer.restart();

  // OOS stands for "out-of-sample"
  constexpr size_t N_OOS_POINTS = 10;
  const auto oos_points = generate_oos_points_doublewell(N_OOS_POINTS);
  //const auto oos_points = generate_oos_points_lorenz63(N_OOS_POINTS);

  KAF::PairwiseDistances oos_distances(doublewell_x, oos_points);
  oos_distances.rescale_by(
      kde.get_rho(),
      kde.compute_oos_rho(oos_distances, KNN));

  const auto n_vertical_points   = oos_distances.get_vertical_point_count();
  const auto n_horizontal_points = oos_distances.get_horizontal_point_count();

  // step 1
  auto oos_kernel_matrix =
    kernel_eval.compute_sparse_matrix(oos_distances, 0.5);
  // step 2
  VectorXd oos_d_inverse =
    1.0 / matrix_tool.compute_column_sums(oos_kernel_matrix);
  // step 3
  std::variant<KAF::SparseMatrix, KAF::DenseMatrix> oos_normalized_kernel_matrix;
  if (oos_kernel_matrix.nonZeros() > 0.1 * n_vertical_points * n_horizontal_points) {
    std::cout << "~~~ Dense" << std::endl;
    oos_normalized_kernel_matrix = 
      KAF::DenseMatrix(
          q_sqrt_inverse.asDiagonal() * oos_kernel_matrix * oos_d_inverse.asDiagonal());
  } else {
    std::cout << "~~~ Sparse" << std::endl;
    oos_normalized_kernel_matrix =
      KAF::SparseMatrix(
          q_sqrt_inverse.asDiagonal() * oos_kernel_matrix * oos_d_inverse.asDiagonal());
  }
  // step 4
  KAF::DenseMatrix pointwise_evaluations(N_OOS_POINTS, N_SING);
  if (std::holds_alternative<KAF::DenseMatrix>(oos_normalized_kernel_matrix)) {
    pointwise_evaluations =
      std::get<KAF::DenseMatrix>(oos_normalized_kernel_matrix).transpose() *
      svd.get_right_vectors();
  } else {
    pointwise_evaluations =
      std::get<KAF::SparseMatrix>(oos_normalized_kernel_matrix).transpose() *
      svd.get_right_vectors();
  }

  std::cout << "Pointwise evaluation done: " << timer << std::endl;

  /****************************************************************************/
  constexpr double rkhs_regularizer = 1.0;
  const VectorXd regularized_eigenvalues =
    Eigen::exp(rkhs_regularizer * (1.0 - 1.0 / svd.get_squared_values()));

  const auto fd_generator_stencil = construct_fd_generator_stencil(n_points);

  /****************************************************************************/
  // NOTE: all matrices below are written transposed
  KAF::NumpyReaderWriter numpy;

  if (std::holds_alternative<KAF::DenseMatrix>(normalized_kernel_matrix)) {
    numpy.write("normalized_kernel.npy", std::get<KAF::DenseMatrix>(normalized_kernel_matrix));
  } else {
    numpy.write("normalized_kernel.npy", std::get<KAF::SparseMatrix>(normalized_kernel_matrix));
  }

  numpy.write("singvals.npy", svd.get_values());
  numpy.write("left_singvecs.npy",  svd.get_left_vectors());
  numpy.write("right_singvecs.npy", svd.get_right_vectors());

  numpy.write("pointwise_evaluations.npy", pointwise_evaluations);

  /****************************************************************************/
  return 0;
}


